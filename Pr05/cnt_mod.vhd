----------------------------------------------------------------------------------
-- Company: Universidad Complutense de Madrid
-- Engineer: David Dav Lavia
-- 
-- Create Date:    16:08:19 11/29/2018 
-- Design Name: 	 Contador mdulo K
-- Module Name:    cnt_mod - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use WORK.COMMON.ALL;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

entity cnt_mod is
generic (K : integer := 15);
port (
	clk		: in std_logic;
	rst		: in std_logic;
	clken		: in std_logic;
	
	cnt		: out std_logic_vector(integer(ceil(log2(real(K))))-1 downto 0)
);
end cnt_mod;

architecture Behavioral of cnt_mod is
	constant N_BITS	: integer	:= integer(ceil(log2(real(K))));
	signal cnt_in		: unsigned(N_BITS-1 downto 0) := to_unsigned(0, N_BITS);
--	signal cnt_seg		: std_logic_vector(N_BITS-1 downto 0)	:= std_logic_vector(to_unsigned(0, N_BITS));
begin

--	p_seg	: process (clk, clken, rst)
--	begin
--		if rst = '0' then
--			cnt_seg <= (others => '0');
--		elsif clken = '1' and rising_edge(clk) then
--			cnt_seg <= std_logic_vector(cnt_in);
--		end if;
--	end process p_seg;	
--	cnt <= cnt_seg;
	cnt <= std_logic_vector(cnt_in);

	p_reg : process (clk, rst, clken)
	begin
		if rst = '0' then
			cnt_in <= to_unsigned(0, N_BITS);
		elsif clken = '1' and rising_edge(clk) then
			if cnt_in = K-1 then
				cnt_in <= to_unsigned(0, N_BITS);
			else
				cnt_in <= cnt_in + 1;
			end if;
		end if;
	end process p_reg;
end Behavioral;

