----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:22:43 11/29/2018 
-- Design Name: 
-- Module Name:    slots_cu - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use WORK.COMMON.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity slots_cu is
port(
	clk	: in std_logic;
	rst	: in std_logic;
	inib	: in std_logic;
	endb	: in std_logic;
	
	-- STATUS SIGNALS
	same	: in std_logic;
	seqfin	: in std_logic;
	
	-- CONTROL SIGNALS
	spin		: out std_logic;	-- If se should be spinning
	cntrst	: out std_logic;	-- Resets the loser/winner time counter
	seq		: out t_sequence	-- The sequence to use
);
end slots_cu;

architecture Behavioral of slots_cu is
	type t_status is (s_init, s_spinning, s_stop, s_winner, s_loser);
	signal current_state, next_state	: t_status	:= s_init;
begin

	p_next_state: process(current_state, inib, endb, seqfin, same) is
	begin
		case current_state is
			when s_init =>
				if inib = '1' then
					next_state <= s_spinning;
				else
					next_state <= s_init;
				end if;
			when s_spinning =>
				if endb = '1' then
--					if same = '1' then
--						next_state <= s_winner;
--					else
--						next_state <= s_loser;
--					end if;
					next_state <= s_stop;
				else
					next_state <= s_spinning;
				end if;
			when s_stop =>
				if same = '1' then
					next_state <= s_winner;
				else
					next_state <= s_loser;
				end if;
			when s_loser =>
				if seqfin = '1' then
					next_state <= s_init;
				else
					next_state <= s_loser;
				end if;
			when s_winner =>
				if seqfin = '1' then
					next_state <= s_init;
				else
					next_state <= s_winner;
				end if;
			when others => next_state <= s_init;
		end case;
	end process p_next_state;
	
	p_outputs : process(current_state) is
	begin
		case current_state is
			when s_init =>
				cntrst	<= '0';
				seq		<= seq_wait;
				spin		<= '0';
			when s_spinning =>
				cntrst	<= '0';
				seq 		<= seq_off;
				spin		<= '1';
			when s_stop	 =>
				cntrst	<= '0';
				seq		<= seq_off;
				spin		<= '0';
			when s_loser =>
				cntrst	<= '1';
				seq 		<= seq_lose;
				spin		<= '0';
			when s_winner =>
				cntrst	<= '1';
				seq		<= seq_win;
				spin		<= '0';
		end case;
	end process p_outputs;
	
	p_status_reg : process (clk, rst, next_state) is
	begin
		if rst = '0' then
			current_state <= s_init;
		elsif rising_edge(clk) then
			current_state <= next_state;
		end if;
	end process p_status_reg;
end Behavioral;

