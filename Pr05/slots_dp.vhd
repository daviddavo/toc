----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:33:54 11/29/2018 
-- Design Name: 
-- Module Name:    slots_dp - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

use WORK.COMMON.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity slots_dp is
port (
	-- Memory's clock
	clk_mem	: in std_logic;
	-- Led display clock (1 Hz)
	clk_d		: in std_logic;
	-- Left display's clock
	clk_l		: in std_logic;
	-- Right display's cock
	clk_r		: in std_logic;
	
	-- RESET
	rst		: in std_logic;

	-- CONTROL SIGNALS
	spin		: in std_logic;
	cntrst	: in std_logic;
	seq		: in t_sequence;

	-- STATUS SIGNALS
	same	: out std_logic;
	seqfin	: out std_logic;
	
	-- OUTPUTS
	left	: out std_logic_vector(6 downto 0);
	right	: out	std_logic_vector(6 downto 0);
	leds	: out std_logic_vector(9 downto 0)
);
end slots_dp;

architecture Behavioral of slots_dp is
	-- We have K 'cards' in the deck
	constant K			: integer	:= 10;
	constant N_BITS	: integer	:= integer(ceil(log2(real(K))));

	component cnt_mod
	generic ( K : integer );
	port (
		clk, rst, clken	: in std_logic;
		cnt					: out std_logic_vector(N_BITS-1 downto 0)
	);
	end component;
	
	component rom_left
	port (
		clka		: in std_logic;
		ena		: in std_logic;
		addra		: in std_logic_vector(3 downto 0);
		douta		: out std_logic_vector(3 downto 0)
	);
	end component;
	
	component rom_right
	port (
		clka		: in std_logic;
		ena		: in std_logic;
		addra		: in std_logic_vector(3 downto 0);
		douta		: out std_logic_vector(3 downto 0)
	);
	end component;
	
	component conv_7seg
	port (
		x			: in std_logic_vector(3 downto 0);
		display	: out std_logic_vector(6 downto 0)
	);
	end component;
	
	attribute box_type : string;
	attribute box_type of rom_left : component is "black_box";
	attribute box_type of rom_right : component is "black_box";
	-- This solves "instantiating black box module" warning
	-- by telling Xilinx that this is indeed a black box module
	-- https://www.xilinx.com/support/answers/9838.html
	
	signal cnt_l	: std_logic_vector(N_BITS-1 downto 0);
	signal cnt_r	: std_logic_vector(N_BITS-1 downto 0);
	signal cnt_seq	: std_logic_vector(N_BITS-1 downto 0);
	
	signal mem_l			: std_logic_vector(3 downto 0);
	signal mem_r			: std_logic_vector(3 downto 0);
	
	signal leds_win		: std_logic_vector(9 downto 0)	:= "0000000000";
	signal leds_lose		: std_logic_vector(9 downto 0)	:= "0000000000";
	signal leds_wait		: std_logic_vector(9 downto 0)	:= "0000000000";
	
	signal left_in			: std_logic_vector(6 downto 0)	:=	"0000000";
	signal right_in		: std_logic_vector(6 downto 0)	:= "0000000";
begin
	i_cnt_mod_l : cnt_mod
	generic map (K => K)
	port map (
		rst	=> rst,
		clk	=> clk_l,
		clken	=> spin,
		
		cnt	=> cnt_l
	);

	i_cnt_mod_r	: cnt_mod
	generic map (K => K)
	port map (
		rst	=> rst,
		clk	=> clk_r,
		clken	=> spin,
		
		cnt 	=> cnt_r
	);
	
	i_cnt_seconds : cnt_mod
	generic map (K => 10)
	port map (
		rst	=> cntrst,
		clk	=> clk_d,
		clken	=> '1',	-- Always enabled, we reset it when we have to
		cnt	=> cnt_seq
	);
	
	seqfin <= '1' when unsigned(cnt_seq) = 9 else '0';
	
	i_rom_left	: rom_left
	port map (
		clka	=> clk_mem,
		addra	=> cnt_l,
		douta	=> mem_l,
		ena	=> spin
	);
	
	i_rom_right	: rom_right
	port map (
		clka	=> clk_mem,
		addra	=> cnt_r,
		douta	=> mem_r,
		ena	=> spin
	);
	
	p_leds_wait	: process (rst, clk_d) is
	begin
		if rst = '0' then
			leds_wait <= "0000000000";
		elsif rising_edge(clk_d) then
			leds_wait(8 downto 0) <= leds_wait(9 downto 1);
			leds_wait(9) <= not leds_wait(0);
		end if;
	end process p_leds_wait;
	
	p_leds_lose	: process (rst, clk_d) is
	begin
		if rst = '0' then
			leds_lose <= "1010101010";
		elsif rising_edge(clk_d) then
			leds_lose(8 downto 0) <= leds_lose(9 downto 1);
			leds_lose(9) <= leds_lose(0);
		end if;
	end process p_leds_lose;
	
	p_leds_win	: process (rst, clk_d) is
	begin
		if rst = '0' then
			leds_win	<= "0000000000";
		elsif rising_edge(clk_d) then
			if leds_win(0) = '0' then
				leds_win <= "1111111111";
			else
				leds_win <= "0000000000";
			end if;
		end if;
	end process p_leds_win;
	
	-- This is just like a MUX
	p_leds	: process (seq, leds_win, leds_lose, leds_wait) is
	begin
		case seq is
			when seq_off =>
				leds <= "0000000000";
			when seq_win =>
				leds <= leds_win;
			when seq_lose =>
				leds <= leds_lose;
			when seq_wait =>
				leds <= leds_wait;
		end case;
	end process p_leds;
	
	i_display_left	: conv_7seg
	port map (
		x			=> mem_l,
		display	=> left_in
	);
	
	i_display_right : conv_7seg
	port map (
		x			=> mem_r,
		display	=> right_in
	);
	
	p_displays	: process(seq, left_in, right_in) is
	begin
		case seq is
			when seq_wait	=> 
				left	<= "0000000";
				right	<= "0000000";
			when others =>
				left	<= left_in;
				right	<= right_in;
		end case;
	end process p_displays;
	
	same <= '1' when mem_l = mem_r else '0';
end Behavioral;

