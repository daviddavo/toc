--------------------------------------------------------------------------------
-- Copyright (c) 1995-2013 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: P.20131013
--  \   \         Application: netgen
--  /   /         Filename: slots_top_timesim.vhd
-- /___/   /\     Timestamp: Mon Dec  3 12:29:54 2018
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -intstyle ise -s 5 -pcf slots_top.pcf -rpw 100 -tpw 0 -ar Structure -tm slots_top -insert_pp_buffers true -w -dir netgen/par -ofmt vhdl -sim slots_top.ncd slots_top_timesim.vhd 
-- Device	: 3s1000ft256-5 (PRODUCTION 1.39 2013-10-13)
-- Input file	: slots_top.ncd
-- Output file	: /home/davo/Documents/toc/Pr05/netgen/par/slots_top_timesim.vhd
-- # of Entities	: 1
-- Design Name	: slots_top
-- Xilinx	: /opt/Xilinx/14.7/ISE_DS/ISE/
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library SIMPRIM;
use SIMPRIM.VCOMPONENTS.ALL;
use SIMPRIM.VPACKAGE.ALL;

entity slots_top is
  port (
    clk : in STD_LOGIC := 'X'; 
    rst : in STD_LOGIC := 'X'; 
    inib : in STD_LOGIC := 'X'; 
    endb : in STD_LOGIC := 'X'; 
    right : out STD_LOGIC_VECTOR ( 6 downto 0 ); 
    leds : out STD_LOGIC_VECTOR ( 9 downto 0 ); 
    left : out STD_LOGIC_VECTOR ( 6 downto 0 ) 
  );
end slots_top;

architecture Structure of slots_top is
  signal i_divisor_d_clk_out_reg_891 : STD_LOGIC; 
  signal rst_IBUF_892 : STD_LOGIC; 
  signal N91 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd3_894 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd2_895 : STD_LOGIC; 
  signal i_dp_cntrst_inv_0 : STD_LOGIC; 
  signal seqfin_0 : STD_LOGIC; 
  signal N109 : STD_LOGIC; 
  signal N83 : STD_LOGIC; 
  signal N101 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_917 : STD_LOGIC; 
  signal spin_0 : STD_LOGIC; 
  signal N103 : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_921 : STD_LOGIC; 
  signal clk_BUFGP : STD_LOGIC; 
  signal N89 : STD_LOGIC; 
  signal N107 : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_929 : STD_LOGIC; 
  signal N99 : STD_LOGIC; 
  signal N95 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg1 : STD_LOGIC; 
  signal N54_0 : STD_LOGIC; 
  signal N51_0 : STD_LOGIC; 
  signal N87 : STD_LOGIC; 
  signal N105 : STD_LOGIC; 
  signal N01 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg3_bdd0_0 : STD_LOGIC; 
  signal N97 : STD_LOGIC; 
  signal endb_IBUF_958 : STD_LOGIC; 
  signal inib_IBUF_970 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg_cmp_eq0000_SW0_O : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_0_1_SW0_O : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_cmp_eq0000_SW0_O : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg4_SW0_O : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_3_1_SW1_O : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_4_1_SW0_O : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_cmp_eq0000_SW0_O : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_In96_SW0_O : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_In26_0 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_In53_0 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_5_1_SW1_O : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg_xor_0_1_SW0_O : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg_xor_2_1_SW0_O : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg5_SW0_SW0_O : STD_LOGIC; 
  signal N93 : STD_LOGIC; 
  signal N85 : STD_LOGIC; 
  signal i_dp_leds_win_0_DYMUX_1031 : STD_LOGIC; 
  signal i_dp_leds_win_0_CLKINV_1028 : STD_LOGIC; 
  signal right_2_OBUF_1059 : STD_LOGIC; 
  signal N91_pack_1 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_1_DXMUX_1100 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_Mcount_cnt_in1 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_1_DYMUX_1086 : STD_LOGIC; 
  signal seqfin : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_1_SRINV_1077 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_1_CLKINV_1076 : STD_LOGIC; 
  signal left_0_OBUF_1128 : STD_LOGIC; 
  signal N109_pack_1 : STD_LOGIC; 
  signal right_6_OBUF_1152 : STD_LOGIC; 
  signal N83_pack_1 : STD_LOGIC; 
  signal left_4_OBUF_1176 : STD_LOGIC; 
  signal N101_pack_1 : STD_LOGIC; 
  signal spin : STD_LOGIC; 
  signal i_dp_cntrst_inv : STD_LOGIC; 
  signal left_3_OBUF_1224 : STD_LOGIC; 
  signal N103_pack_1 : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_DYMUX_1236 : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_CLKINV_1233 : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_CEINV_1232 : STD_LOGIC; 
  signal right_3_OBUF_1265 : STD_LOGIC; 
  signal N89_pack_1 : STD_LOGIC; 
  signal left_1_OBUF_1289 : STD_LOGIC; 
  signal N107_pack_1 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_0_DXMUX_1321 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_0_DYMUX_1312 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_Mcount_cnt_in1 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_0_SRINVNOT : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_0_CLKINV_1303 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_0_CEINV_1302 : STD_LOGIC; 
  signal left_5_OBUF_1350 : STD_LOGIC; 
  signal N99_pack_1 : STD_LOGIC; 
  signal right_0_OBUF_1374 : STD_LOGIC; 
  signal N95_pack_1 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg1_DYMUX_1386 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg1_CLKINV_1383 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg1_CEINV_1382 : STD_LOGIC; 
  signal N54 : STD_LOGIC; 
  signal N51 : STD_LOGIC; 
  signal right_4_OBUF_1439 : STD_LOGIC; 
  signal N87_pack_1 : STD_LOGIC; 
  signal left_2_OBUF_1463 : STD_LOGIC; 
  signal N105_pack_1 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_1_DXMUX_1494 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg1 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg3_bdd0 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_1_CLKINV_1476 : STD_LOGIC; 
  signal left_6_OBUF_1522 : STD_LOGIC; 
  signal N97_pack_1 : STD_LOGIC; 
  signal clk_INBUF : STD_LOGIC; 
  signal right_0_O : STD_LOGIC; 
  signal right_1_O : STD_LOGIC; 
  signal right_2_O : STD_LOGIC; 
  signal right_3_O : STD_LOGIC; 
  signal endb_INBUF : STD_LOGIC; 
  signal right_4_O : STD_LOGIC; 
  signal leds_0_O : STD_LOGIC; 
  signal right_5_O : STD_LOGIC; 
  signal leds_1_O : STD_LOGIC; 
  signal right_6_O : STD_LOGIC; 
  signal leds_2_O : STD_LOGIC; 
  signal leds_3_O : STD_LOGIC; 
  signal leds_4_O : STD_LOGIC; 
  signal leds_5_O : STD_LOGIC; 
  signal leds_6_O : STD_LOGIC; 
  signal leds_7_O : STD_LOGIC; 
  signal leds_8_O : STD_LOGIC; 
  signal leds_9_O : STD_LOGIC; 
  signal left_0_O : STD_LOGIC; 
  signal inib_INBUF : STD_LOGIC; 
  signal left_1_O : STD_LOGIC; 
  signal left_2_O : STD_LOGIC; 
  signal left_3_O : STD_LOGIC; 
  signal left_4_O : STD_LOGIC; 
  signal left_5_O : STD_LOGIC; 
  signal left_6_O : STD_LOGIC; 
  signal rst_INBUF : STD_LOGIC; 
  signal clk_BUFGP_BUFG_S_INVNOT : STD_LOGIC; 
  signal clk_BUFGP_BUFG_I0_INV : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg_BUFG_S_INVNOT : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg_BUFG_I0_INV : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB3 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB2 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB1 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB0 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB31 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB30 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB29 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB28 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB27 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB26 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB25 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB24 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB23 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB22 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB21 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB20 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB19 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB18 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB17 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB15 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB14 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB13 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB12 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB11 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB10 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB9 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB8 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB7 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB6 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB5 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB4 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB3 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB2 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB1 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA3 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA2 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA1 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA0 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA31 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA30 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA29 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA28 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA27 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA26 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA25 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA24 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA23 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA22 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA21 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA20 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA19 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA18 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA17 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA15 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA14 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA13 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA12 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA11 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA10 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA9 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA8 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA7 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA6 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA5 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA4 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA3 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA2 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA1 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA3 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA2 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA1 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA0 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA31 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA30 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA29 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA28 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA27 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA26 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA25 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA24 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA23 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA22 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA21 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA20 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA19 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA18 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA17 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA16 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA15 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA14 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA13 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA12 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA11 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA10 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA9 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA8 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA7 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA6 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA5 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA4 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA3 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA2 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA1 : STD_LOGIC; 
  signal i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA0 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB3 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB2 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB1 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB0 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB31 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB30 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB29 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB28 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB27 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB26 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB25 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB24 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB23 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB22 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB21 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB20 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB19 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB18 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB17 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB15 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB14 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB13 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB12 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB11 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB10 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB9 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB8 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB7 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB6 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB5 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB4 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB3 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB2 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB1 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA3 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA2 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA1 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA0 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA31 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA30 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA29 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA28 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA27 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA26 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA25 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA24 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA23 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA22 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA21 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA20 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA19 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA18 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA17 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA15 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA14 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA13 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA12 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA11 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA10 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA9 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA8 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA7 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA6 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA5 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA4 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA3 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA2 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA1 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA3 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA2 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA1 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA0 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA31 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA30 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA29 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA28 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA27 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA26 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA25 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA24 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA23 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA22 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA21 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA20 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA19 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA18 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA17 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA16 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA15 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA14 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA13 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA12 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA11 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA10 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA9 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA8 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA7 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA6 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA5 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA4 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA3 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA2 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA1 : STD_LOGIC; 
  signal i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA0 : STD_LOGIC; 
  signal leds_1_OBUF_F5MUX_2114 : STD_LOGIC; 
  signal N130 : STD_LOGIC; 
  signal leds_1_OBUF_BXINV_2107 : STD_LOGIC; 
  signal N129 : STD_LOGIC; 
  signal leds_0_OBUF_F5MUX_2139 : STD_LOGIC; 
  signal N128 : STD_LOGIC; 
  signal leds_0_OBUF_BXINV_2132 : STD_LOGIC; 
  signal N127 : STD_LOGIC; 
  signal leds_2_OBUF_F5MUX_2164 : STD_LOGIC; 
  signal N126 : STD_LOGIC; 
  signal leds_2_OBUF_BXINV_2157 : STD_LOGIC; 
  signal N125 : STD_LOGIC; 
  signal leds_3_OBUF_F5MUX_2189 : STD_LOGIC; 
  signal N124 : STD_LOGIC; 
  signal leds_3_OBUF_BXINV_2182 : STD_LOGIC; 
  signal N123 : STD_LOGIC; 
  signal leds_4_OBUF_F5MUX_2214 : STD_LOGIC; 
  signal N122 : STD_LOGIC; 
  signal leds_4_OBUF_BXINV_2207 : STD_LOGIC; 
  signal N121 : STD_LOGIC; 
  signal leds_5_OBUF_F5MUX_2239 : STD_LOGIC; 
  signal N120 : STD_LOGIC; 
  signal leds_5_OBUF_BXINV_2232 : STD_LOGIC; 
  signal N119 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd2_DXMUX_2270 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd2_F5MUX_2268 : STD_LOGIC; 
  signal N132 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd2_BXINV_2261 : STD_LOGIC; 
  signal N131 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd2_CLKINV_2252 : STD_LOGIC; 
  signal leds_6_OBUF_F5MUX_2300 : STD_LOGIC; 
  signal N118 : STD_LOGIC; 
  signal leds_6_OBUF_BXINV_2293 : STD_LOGIC; 
  signal N117 : STD_LOGIC; 
  signal leds_7_OBUF_F5MUX_2325 : STD_LOGIC; 
  signal N116 : STD_LOGIC; 
  signal leds_7_OBUF_BXINV_2318 : STD_LOGIC; 
  signal N115 : STD_LOGIC; 
  signal leds_8_OBUF_F5MUX_2350 : STD_LOGIC; 
  signal N114 : STD_LOGIC; 
  signal leds_8_OBUF_BXINV_2343 : STD_LOGIC; 
  signal N113 : STD_LOGIC; 
  signal leds_9_OBUF_F5MUX_2375 : STD_LOGIC; 
  signal N112 : STD_LOGIC; 
  signal leds_9_OBUF_BXINV_2368 : STD_LOGIC; 
  signal N111 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg_cmp_eq0000_2398 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg_cmp_eq0000_SW0_O_pack_1 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_0_DXMUX_2429 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg : STD_LOGIC; 
  signal N01_pack_2 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_0_CLKINV_2412 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_0_DXMUX_2464 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_0_1_SW0_O_pack_2 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_0_CLKINV_2447 : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_cmp_eq0000_2492 : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_cmp_eq0000_SW0_O_pack_1 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_4_DXMUX_2523 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg4_2520 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg4_SW0_O_pack_2 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_4_CLKINV_2507 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_3_DXMUX_2558 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg3 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_3_1_SW1_O_pack_2 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_3_CLKINV_2542 : STD_LOGIC; 
  signal i_dp_leds_win_0_FFY_RSTAND_1036 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_4_DXMUX_2593 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg4 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_4_1_SW0_O_pack_2 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_4_CLKINV_2575 : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_cmp_eq0000_2621 : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_cmp_eq0000_SW0_O_pack_1 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_DXMUX_2652 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_In : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_In96_SW0_O_pack_2 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_CLKINV_2635 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_5_DXMUX_2687 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg5 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg_xor_5_1_SW1_O_pack_2 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_5_CLKINV_2671 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_0_DXMUX_2722 : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg_xor_0_1_SW0_O_pack_2 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_0_CLKINV_2704 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_2_DXMUX_2757 : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg2 : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg_xor_2_1_SW0_O_pack_2 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_2_CLKINV_2739 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_5_DXMUX_2794 : STD_LOGIC; 
  signal N57 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg5_SW0_SW0_O_pack_3 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_5_CLKINV_2777 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_5_CEINV_2776 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_3_DXMUX_2837 : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg3 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_3_DYMUX_2823 : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg1 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_3_SRINVNOT : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_3_CLKINV_2812 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_3_DXMUX_2882 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_Mcount_cnt_in3 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_3_DYMUX_2867 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_Mcount_cnt_in2 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_3_SRINVNOT : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_3_CLKINV_2857 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_3_CEINV_2856 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_4_DYMUX_2908 : STD_LOGIC; 
  signal i_divisor_d_Mcount_cntr_reg_xor_4_1 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_4_CLKINV_2899 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_4_CEINV_2898 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_2_DXMUX_2951 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg2 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_2_DYMUX_2936 : STD_LOGIC; 
  signal i_divisor_l_Mcount_cntr_reg1 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_2_SRINVNOT : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_2_CLKINV_2925 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_3_DXMUX_2996 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_Mcount_cnt_in3 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_3_DYMUX_2981 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_Mcount_cnt_in2 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_3_SRINVNOT : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_3_CLKINV_2971 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_l_cnt_in_3_CEINV_2970 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd3_DYMUX_3020 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd3_In : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd3_CLKINV_3010 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_3_DXMUX_3062 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_Mcount_cnt_in3 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_3_DYMUX_3048 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_Mcount_cnt_in2 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_3_SRINV_3039 : STD_LOGIC; 
  signal i_dp_i_cnt_seconds_cnt_in_3_CLKINV_3038 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_3_DXMUX_3104 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg3 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_3_DYMUX_3090 : STD_LOGIC; 
  signal i_divisor_r_Mcount_cntr_reg2 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_3_SRINVNOT : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_3_CLKINV_3080 : STD_LOGIC; 
  signal right_1_OBUF_3132 : STD_LOGIC; 
  signal N93_pack_1 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_0_DXMUX_3164 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_0_DYMUX_3155 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_Mcount_cnt_in1 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_0_SRINVNOT : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_0_CLKINV_3146 : STD_LOGIC; 
  signal i_dp_i_cnt_mod_r_cnt_in_0_CEINV_3145 : STD_LOGIC; 
  signal i_dp_leds_lose_1_DXMUX_3189 : STD_LOGIC; 
  signal i_dp_leds_lose_1_DYMUX_3181 : STD_LOGIC; 
  signal i_dp_leds_lose_1_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_lose_1_CLKINV_3178 : STD_LOGIC; 
  signal right_5_OBUF_3216 : STD_LOGIC; 
  signal N85_pack_1 : STD_LOGIC; 
  signal i_dp_leds_lose_3_DXMUX_3236 : STD_LOGIC; 
  signal i_dp_leds_lose_3_DYMUX_3228 : STD_LOGIC; 
  signal i_dp_leds_lose_3_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_lose_3_CLKINV_3225 : STD_LOGIC; 
  signal i_dp_leds_lose_5_DXMUX_3259 : STD_LOGIC; 
  signal i_dp_leds_lose_5_DYMUX_3251 : STD_LOGIC; 
  signal i_dp_leds_lose_5_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_lose_5_CLKINV_3248 : STD_LOGIC; 
  signal i_dp_leds_lose_7_DXMUX_3282 : STD_LOGIC; 
  signal i_dp_leds_lose_7_DYMUX_3274 : STD_LOGIC; 
  signal i_dp_leds_lose_7_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_lose_7_CLKINV_3271 : STD_LOGIC; 
  signal i_dp_leds_wait_1_DXMUX_3305 : STD_LOGIC; 
  signal i_dp_leds_wait_1_DYMUX_3297 : STD_LOGIC; 
  signal i_dp_leds_wait_1_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_wait_1_CLKINV_3294 : STD_LOGIC; 
  signal i_dp_leds_lose_9_DXMUX_3329 : STD_LOGIC; 
  signal i_dp_leds_lose_9_DYMUX_3321 : STD_LOGIC; 
  signal i_dp_leds_lose_9_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_lose_9_CLKINV_3318 : STD_LOGIC; 
  signal i_dp_leds_wait_3_DXMUX_3352 : STD_LOGIC; 
  signal i_dp_leds_wait_3_DYMUX_3344 : STD_LOGIC; 
  signal i_dp_leds_wait_3_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_wait_3_CLKINV_3341 : STD_LOGIC; 
  signal i_dp_leds_wait_5_DXMUX_3376 : STD_LOGIC; 
  signal i_dp_leds_wait_5_DYMUX_3368 : STD_LOGIC; 
  signal i_dp_leds_wait_5_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_wait_5_CLKINV_3365 : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_DYMUX_3392 : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_CLKINV_3389 : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_CEINV_3388 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_In26_3409 : STD_LOGIC; 
  signal i_dp_leds_wait_7_DXMUX_3429 : STD_LOGIC; 
  signal i_dp_leds_wait_7_DYMUX_3421 : STD_LOGIC; 
  signal i_dp_leds_wait_7_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_wait_7_CLKINV_3418 : STD_LOGIC; 
  signal i_dp_leds_wait_9_DXMUX_3453 : STD_LOGIC; 
  signal i_dp_leds_wait_9_DYMUX_3445 : STD_LOGIC; 
  signal i_dp_leds_wait_9_SRINVNOT : STD_LOGIC; 
  signal i_dp_leds_wait_9_CLKINV_3442 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_In53_3469 : STD_LOGIC; 
  signal i_divisor_d_clk_out_reg1_FFY_RSTAND_1392 : STD_LOGIC; 
  signal i_divisor_r_clk_out_reg_FFY_RSTAND_1242 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_1_FFX_RSTAND_1499 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd2_FFX_RSTAND_2275 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_0_FFX_RSTAND_2434 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_0_FFX_RSTAND_2469 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_4_FFX_RSTAND_2528 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_3_FFX_RSTAND_2563 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_4_FFX_RSTAND_2598 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_4_FFY_RSTAND_2914 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_2_FFX_RSTAND_2762 : STD_LOGIC; 
  signal i_divisor_r_cntr_reg_5_FFX_RSTAND_2800 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd1_FFX_RSTAND_2657 : STD_LOGIC; 
  signal i_divisor_l_cntr_reg_5_FFX_RSTAND_2692 : STD_LOGIC; 
  signal i_divisor_d_cntr_reg_0_FFX_RSTAND_2727 : STD_LOGIC; 
  signal i_cu_current_state_FSM_FFd3_FFY_RSTAND_3025 : STD_LOGIC; 
  signal i_divisor_l_clk_out_reg_FFY_RSTAND_3398 : STD_LOGIC; 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q : STD_LOGIC;
 
  signal NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q : STD_LOGIC;
 
  signal VCC : STD_LOGIC; 
  signal GND : STD_LOGIC; 
  signal i_dp_leds_win : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal i_dp_mem_r : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal i_dp_i_cnt_seconds_cnt_in : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal i_dp_mem_l : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal i_dp_i_cnt_mod_l_cnt_in : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal i_divisor_l_cntr_reg : STD_LOGIC_VECTOR ( 5 downto 0 ); 
  signal i_divisor_r_cntr_reg : STD_LOGIC_VECTOR ( 5 downto 0 ); 
  signal i_dp_i_cnt_mod_r_cnt_in : STD_LOGIC_VECTOR ( 3 downto 0 ); 
  signal i_dp_leds_lose : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal i_dp_leds_wait : STD_LOGIC_VECTOR ( 9 downto 0 ); 
  signal i_divisor_d_cntr_reg : STD_LOGIC_VECTOR ( 4 downto 0 ); 
begin
  i_dp_leds_win_0_DYMUX : X_INV
    generic map(
      LOC => "SLICE_X52Y80",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_win(0),
      O => i_dp_leds_win_0_DYMUX_1031
    );
  i_dp_leds_win_0_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X52Y80",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_win_0_CLKINV_1028
    );
  right_2_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X45Y95",
      PATHPULSE => 526 ps
    )
    port map (
      I => N91_pack_1,
      O => N91
    );
  i_dp_right_2_11_SW0 : X_LUT4
    generic map(
      INIT => X"A120",
      LOC => "SLICE_X45Y95"
    )
    port map (
      ADR0 => i_dp_mem_r(2),
      ADR1 => i_dp_mem_r(0),
      ADR2 => i_dp_mem_r(3),
      ADR3 => i_dp_mem_r(1),
      O => N91_pack_1
    );
  i_dp_i_cnt_seconds_cnt_in_1_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X43Y72",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_seconds_Mcount_cnt_in1,
      O => i_dp_i_cnt_seconds_cnt_in_1_DXMUX_1100
    );
  i_dp_i_cnt_seconds_cnt_in_1_DYMUX : X_INV
    generic map(
      LOC => "SLICE_X43Y72",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_seconds_cnt_in(0),
      O => i_dp_i_cnt_seconds_cnt_in_1_DYMUX_1086
    );
  i_dp_i_cnt_seconds_cnt_in_1_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X43Y72",
      PATHPULSE => 526 ps
    )
    port map (
      I => seqfin,
      O => seqfin_0
    );
  i_dp_i_cnt_seconds_cnt_in_1_SRINV : X_BUF
    generic map(
      LOC => "SLICE_X43Y72",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_cntrst_inv_0,
      O => i_dp_i_cnt_seconds_cnt_in_1_SRINV_1077
    );
  i_dp_i_cnt_seconds_cnt_in_1_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X43Y72",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_i_cnt_seconds_cnt_in_1_CLKINV_1076
    );
  i_dp_seqfin1 : X_LUT4
    generic map(
      INIT => X"0008",
      LOC => "SLICE_X43Y72"
    )
    port map (
      ADR0 => i_dp_i_cnt_seconds_cnt_in(3),
      ADR1 => i_dp_i_cnt_seconds_cnt_in(0),
      ADR2 => i_dp_i_cnt_seconds_cnt_in(1),
      ADR3 => i_dp_i_cnt_seconds_cnt_in(2),
      O => seqfin
    );
  left_0_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X38Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => N109_pack_1,
      O => N109
    );
  i_dp_left_0_11_SW0 : X_LUT4
    generic map(
      INIT => X"B7EB",
      LOC => "SLICE_X38Y85"
    )
    port map (
      ADR0 => i_dp_mem_l(1),
      ADR1 => i_dp_mem_l(0),
      ADR2 => i_dp_mem_l(2),
      ADR3 => i_dp_mem_l(3),
      O => N109_pack_1
    );
  right_6_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X42Y94",
      PATHPULSE => 526 ps
    )
    port map (
      I => N83_pack_1,
      O => N83
    );
  i_dp_right_6_11_SW0 : X_LUT4
    generic map(
      INIT => X"F7DA",
      LOC => "SLICE_X42Y94"
    )
    port map (
      ADR0 => i_dp_mem_r(2),
      ADR1 => i_dp_mem_r(0),
      ADR2 => i_dp_mem_r(3),
      ADR3 => i_dp_mem_r(1),
      O => N83_pack_1
    );
  left_4_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X37Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => N101_pack_1,
      O => N101
    );
  i_dp_left_4_11_SW0 : X_LUT4
    generic map(
      INIT => X"AE8F",
      LOC => "SLICE_X37Y85"
    )
    port map (
      ADR0 => i_dp_mem_l(3),
      ADR1 => i_dp_mem_l(1),
      ADR2 => i_dp_mem_l(0),
      ADR3 => i_dp_mem_l(2),
      O => N101_pack_1
    );
  spin_XUSED : X_BUF
    generic map(
      LOC => "SLICE_X42Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => spin,
      O => spin_0
    );
  spin_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X42Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_cntrst_inv,
      O => i_dp_cntrst_inv_0
    );
  i_dp_cntrst_inv1 : X_LUT4
    generic map(
      INIT => X"0C0F",
      LOC => "SLICE_X42Y84"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => i_cu_current_state_FSM_FFd1_917,
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => i_dp_cntrst_inv
    );
  left_3_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X36Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => N103_pack_1,
      O => N103
    );
  i_dp_left_3_21_SW0 : X_LUT4
    generic map(
      INIT => X"9086",
      LOC => "SLICE_X36Y85"
    )
    port map (
      ADR0 => i_dp_mem_l(2),
      ADR1 => i_dp_mem_l(0),
      ADR2 => i_dp_mem_l(1),
      ADR3 => i_dp_mem_l(3),
      O => N103_pack_1
    );
  i_divisor_r_clk_out_reg_DYMUX : X_INV
    generic map(
      LOC => "SLICE_X44Y58",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_clk_out_reg_921,
      O => i_divisor_r_clk_out_reg_DYMUX_1236
    );
  i_divisor_r_clk_out_reg_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X44Y58",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_r_clk_out_reg_CLKINV_1233
    );
  i_divisor_r_clk_out_reg_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X44Y58",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_clk_out_reg_cmp_eq0000_2621,
      O => i_divisor_r_clk_out_reg_CEINV_1232
    );
  right_3_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X45Y94",
      PATHPULSE => 526 ps
    )
    port map (
      I => N89_pack_1,
      O => N89
    );
  i_dp_right_3_11_SW0 : X_LUT4
    generic map(
      INIT => X"8492",
      LOC => "SLICE_X45Y94"
    )
    port map (
      ADR0 => i_dp_mem_r(2),
      ADR1 => i_dp_mem_r(1),
      ADR2 => i_dp_mem_r(0),
      ADR3 => i_dp_mem_r(3),
      O => N89_pack_1
    );
  left_1_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X39Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => N107_pack_1,
      O => N107
    );
  i_dp_left_1_11_SW0 : X_LUT4
    generic map(
      INIT => X"AC48",
      LOC => "SLICE_X39Y85"
    )
    port map (
      ADR0 => i_dp_mem_l(3),
      ADR1 => i_dp_mem_l(2),
      ADR2 => i_dp_mem_l(0),
      ADR3 => i_dp_mem_l(1),
      O => N107_pack_1
    );
  i_dp_i_cnt_mod_l_cnt_in_0_DXMUX : X_INV
    generic map(
      LOC => "SLICE_X37Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(0),
      O => i_dp_i_cnt_mod_l_cnt_in_0_DXMUX_1321
    );
  i_dp_i_cnt_mod_l_cnt_in_0_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X37Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_Mcount_cnt_in1,
      O => i_dp_i_cnt_mod_l_cnt_in_0_DYMUX_1312
    );
  i_dp_i_cnt_mod_l_cnt_in_0_SRINV : X_INV
    generic map(
      LOC => "SLICE_X37Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_i_cnt_mod_l_cnt_in_0_SRINVNOT
    );
  i_dp_i_cnt_mod_l_cnt_in_0_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X37Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_clk_out_reg_929,
      O => i_dp_i_cnt_mod_l_cnt_in_0_CLKINV_1303
    );
  i_dp_i_cnt_mod_l_cnt_in_0_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X37Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => spin_0,
      O => i_dp_i_cnt_mod_l_cnt_in_0_CEINV_1302
    );
  left_5_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X35Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => N99_pack_1,
      O => N99
    );
  i_dp_left_5_11_SW0 : X_LUT4
    generic map(
      INIT => X"9FAB",
      LOC => "SLICE_X35Y84"
    )
    port map (
      ADR0 => i_dp_mem_l(3),
      ADR1 => i_dp_mem_l(1),
      ADR2 => i_dp_mem_l(0),
      ADR3 => i_dp_mem_l(2),
      O => N99_pack_1
    );
  right_0_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X46Y95",
      PATHPULSE => 526 ps
    )
    port map (
      I => N95_pack_1,
      O => N95
    );
  i_dp_right_0_11_SW0 : X_LUT4
    generic map(
      INIT => X"B6FD",
      LOC => "SLICE_X46Y95"
    )
    port map (
      ADR0 => i_dp_mem_r(2),
      ADR1 => i_dp_mem_r(3),
      ADR2 => i_dp_mem_r(1),
      ADR3 => i_dp_mem_r(0),
      O => N95_pack_1
    );
  i_divisor_d_clk_out_reg1_DYMUX : X_INV
    generic map(
      LOC => "SLICE_X38Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg1,
      O => i_divisor_d_clk_out_reg1_DYMUX_1386
    );
  i_divisor_d_clk_out_reg1_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X38Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_d_clk_out_reg1_CLKINV_1383
    );
  i_divisor_d_clk_out_reg1_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X38Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_cmp_eq0000_2398,
      O => i_divisor_d_clk_out_reg1_CEINV_1382
    );
  N54_XUSED : X_BUF
    generic map(
      LOC => "SLICE_X29Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => N54,
      O => N54_0
    );
  N54_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X29Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => N51,
      O => N51_0
    );
  i_divisor_l_Mcount_cntr_reg_xor_3_1_SW0 : X_LUT4
    generic map(
      INIT => X"9555",
      LOC => "SLICE_X29Y50"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(3),
      ADR1 => i_divisor_l_cntr_reg(2),
      ADR2 => i_divisor_l_cntr_reg(0),
      ADR3 => i_divisor_l_cntr_reg(1),
      O => N51
    );
  right_4_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X44Y94",
      PATHPULSE => 526 ps
    )
    port map (
      I => N87_pack_1,
      O => N87
    );
  i_dp_right_4_11_SW0 : X_LUT4
    generic map(
      INIT => X"BAB3",
      LOC => "SLICE_X44Y94"
    )
    port map (
      ADR0 => i_dp_mem_r(3),
      ADR1 => i_dp_mem_r(0),
      ADR2 => i_dp_mem_r(1),
      ADR3 => i_dp_mem_r(2),
      O => N87_pack_1
    );
  left_2_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X37Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => N105_pack_1,
      O => N105
    );
  i_dp_left_2_11_SW0 : X_LUT4
    generic map(
      INIT => X"8A04",
      LOC => "SLICE_X37Y84"
    )
    port map (
      ADR0 => i_dp_mem_l(3),
      ADR1 => i_dp_mem_l(1),
      ADR2 => i_dp_mem_l(0),
      ADR3 => i_dp_mem_l(2),
      O => N105_pack_1
    );
  i_divisor_r_cntr_reg_1_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X49Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg1,
      O => i_divisor_r_cntr_reg_1_DXMUX_1494
    );
  i_divisor_r_cntr_reg_1_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X49Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg3_bdd0,
      O => i_divisor_r_Mcount_cntr_reg3_bdd0_0
    );
  i_divisor_r_cntr_reg_1_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X49Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_r_cntr_reg_1_CLKINV_1476
    );
  i_divisor_r_Mcount_cntr_reg311 : X_LUT4
    generic map(
      INIT => X"77FF",
      LOC => "SLICE_X49Y53"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(1),
      ADR1 => i_divisor_r_cntr_reg(2),
      ADR2 => VCC,
      ADR3 => i_divisor_r_cntr_reg(0),
      O => i_divisor_r_Mcount_cntr_reg3_bdd0
    );
  left_6_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X35Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => N97_pack_1,
      O => N97
    );
  i_dp_left_6_11_SW0 : X_LUT4
    generic map(
      INIT => X"BDEE",
      LOC => "SLICE_X35Y85"
    )
    port map (
      ADR0 => i_dp_mem_l(3),
      ADR1 => i_dp_mem_l(1),
      ADR2 => i_dp_mem_l(0),
      ADR3 => i_dp_mem_l(2),
      O => N97_pack_1
    );
  clk_BUFGP_IBUFG : X_BUF
    generic map(
      LOC => "PAD246",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk,
      O => clk_INBUF
    );
  right_0_OBUF : X_OBUF
    generic map(
      LOC => "PAD60"
    )
    port map (
      I => right_0_O,
      O => right(0)
    );
  right_1_OBUF : X_OBUF
    generic map(
      LOC => "PAD59"
    )
    port map (
      I => right_1_O,
      O => right(1)
    );
  right_2_OBUF : X_OBUF
    generic map(
      LOC => "PAD58"
    )
    port map (
      I => right_2_O,
      O => right(2)
    );
  right_3_OBUF : X_OBUF
    generic map(
      LOC => "PAD57"
    )
    port map (
      I => right_3_O,
      O => right(3)
    );
  endb_IBUF : X_BUF
    generic map(
      LOC => "PAD53",
      PATHPULSE => 526 ps
    )
    port map (
      I => endb,
      O => endb_INBUF
    );
  endb_IFF_IMUX : X_BUF
    generic map(
      LOC => "PAD53",
      PATHPULSE => 526 ps
    )
    port map (
      I => endb_INBUF,
      O => endb_IBUF_958
    );
  right_4_OBUF : X_OBUF
    generic map(
      LOC => "PAD56"
    )
    port map (
      I => right_4_O,
      O => right(4)
    );
  leds_0_OBUF : X_OBUF
    generic map(
      LOC => "PAD113"
    )
    port map (
      I => leds_0_O,
      O => leds(0)
    );
  right_5_OBUF : X_OBUF
    generic map(
      LOC => "PAD55"
    )
    port map (
      I => right_5_O,
      O => right(5)
    );
  leds_1_OBUF : X_OBUF
    generic map(
      LOC => "PAD114"
    )
    port map (
      I => leds_1_O,
      O => leds(1)
    );
  right_6_OBUF : X_OBUF
    generic map(
      LOC => "PAD54"
    )
    port map (
      I => right_6_O,
      O => right(6)
    );
  leds_2_OBUF : X_OBUF
    generic map(
      LOC => "PAD83"
    )
    port map (
      I => leds_2_O,
      O => leds(2)
    );
  leds_3_OBUF : X_OBUF
    generic map(
      LOC => "PAD82"
    )
    port map (
      I => leds_3_O,
      O => leds(3)
    );
  leds_4_OBUF : X_OBUF
    generic map(
      LOC => "PAD66"
    )
    port map (
      I => leds_4_O,
      O => leds(4)
    );
  leds_5_OBUF : X_OBUF
    generic map(
      LOC => "PAD65"
    )
    port map (
      I => leds_5_O,
      O => leds(5)
    );
  leds_6_OBUF : X_OBUF
    generic map(
      LOC => "PAD64"
    )
    port map (
      I => leds_6_O,
      O => leds(6)
    );
  leds_7_OBUF : X_OBUF
    generic map(
      LOC => "PAD63"
    )
    port map (
      I => leds_7_O,
      O => leds(7)
    );
  leds_8_OBUF : X_OBUF
    generic map(
      LOC => "PAD62"
    )
    port map (
      I => leds_8_O,
      O => leds(8)
    );
  leds_9_OBUF : X_OBUF
    generic map(
      LOC => "PAD61"
    )
    port map (
      I => leds_9_O,
      O => leds(9)
    );
  left_0_OBUF : X_OBUF
    generic map(
      LOC => "PAD50"
    )
    port map (
      I => left_0_O,
      O => left(0)
    );
  inib_IBUF : X_BUF
    generic map(
      LOC => "PAD51",
      PATHPULSE => 526 ps
    )
    port map (
      I => inib,
      O => inib_INBUF
    );
  inib_IFF_IMUX : X_BUF
    generic map(
      LOC => "PAD51",
      PATHPULSE => 526 ps
    )
    port map (
      I => inib_INBUF,
      O => inib_IBUF_970
    );
  left_1_OBUF : X_OBUF
    generic map(
      LOC => "PAD49"
    )
    port map (
      I => left_1_O,
      O => left(1)
    );
  left_2_OBUF : X_OBUF
    generic map(
      LOC => "PAD48"
    )
    port map (
      I => left_2_O,
      O => left(2)
    );
  left_3_OBUF : X_OBUF
    generic map(
      LOC => "PAD47"
    )
    port map (
      I => left_3_O,
      O => left(3)
    );
  left_4_OBUF : X_OBUF
    generic map(
      LOC => "PAD46"
    )
    port map (
      I => left_4_O,
      O => left(4)
    );
  left_5_OBUF : X_OBUF
    generic map(
      LOC => "PAD45"
    )
    port map (
      I => left_5_O,
      O => left(5)
    );
  left_6_OBUF : X_OBUF
    generic map(
      LOC => "PAD44"
    )
    port map (
      I => left_6_O,
      O => left(6)
    );
  rst_IBUF : X_BUF
    generic map(
      LOC => "PAD52",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst,
      O => rst_INBUF
    );
  rst_IFF_IMUX : X_BUF
    generic map(
      LOC => "PAD52",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_INBUF,
      O => rst_IBUF_892
    );
  clk_BUFGP_BUFG : X_BUFGMUX
    generic map(
      LOC => "BUFGMUX0"
    )
    port map (
      I0 => clk_BUFGP_BUFG_I0_INV,
      I1 => GND,
      S => clk_BUFGP_BUFG_S_INVNOT,
      O => clk_BUFGP
    );
  clk_BUFGP_BUFG_SINV : X_INV
    generic map(
      LOC => "BUFGMUX0",
      PATHPULSE => 526 ps
    )
    port map (
      I => '1',
      O => clk_BUFGP_BUFG_S_INVNOT
    );
  clk_BUFGP_BUFG_I0_USED : X_BUF
    generic map(
      LOC => "BUFGMUX0",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_INBUF,
      O => clk_BUFGP_BUFG_I0_INV
    );
  i_divisor_d_clk_out_reg_BUFG : X_BUFGMUX
    generic map(
      LOC => "BUFGMUX3"
    )
    port map (
      I0 => i_divisor_d_clk_out_reg_BUFG_I0_INV,
      I1 => GND,
      S => i_divisor_d_clk_out_reg_BUFG_S_INVNOT,
      O => i_divisor_d_clk_out_reg_891
    );
  i_divisor_d_clk_out_reg_BUFG_SINV : X_INV
    generic map(
      LOC => "BUFGMUX3",
      PATHPULSE => 526 ps
    )
    port map (
      I => '1',
      O => i_divisor_d_clk_out_reg_BUFG_S_INVNOT
    );
  i_divisor_d_clk_out_reg_BUFG_I0_USED : X_BUF
    generic map(
      LOC => "BUFGMUX3",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg1,
      O => i_divisor_d_clk_out_reg_BUFG_I0_INV
    );
  i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram : X_RAMB16_S36_S36
    generic map(
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      SIM_COLLISION_CHECK => "ALL",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      INIT_00 => X"0000000000000001000000000000000000010000000000000000000100010001",
      INIT_01 => X"0000000100000000000000000001000000000001000100000000000100000001",
      INIT_02 => X"0000000000000000000000000000000000000000000100010001000000000001",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      LOC => "RAMB16_X1Y9",
      SETUP_ALL => 421 ps,
      SETUP_READ_FIRST => 421 ps
    )
    port map (
      CLKA => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA,
      CLKB => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB,
      ENA => spin_0,
      ENB => spin_0,
      SSRA => '0',
      SSRB => '0',
      WEA => '0',
      WEB => '0',
      ADDRA(8) => '0',
      ADDRA(7) => '0',
      ADDRA(6) => '0',
      ADDRA(5) => '0',
      ADDRA(4) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q,
      ADDRA(3) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q,
      ADDRA(2) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q,
      ADDRA(1) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q,
      ADDRA(0) => '0',
      ADDRB(8) => '0',
      ADDRB(7) => '0',
      ADDRB(6) => '0',
      ADDRB(5) => '0',
      ADDRB(4) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q,
      ADDRB(3) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q,
      ADDRB(2) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q,
      ADDRB(1) => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q,
      ADDRB(0) => '1',
      DIA(31) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA31,
      DIA(30) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA30,
      DIA(29) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA29,
      DIA(28) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA28,
      DIA(27) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA27,
      DIA(26) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA26,
      DIA(25) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA25,
      DIA(24) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA24,
      DIA(23) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA23,
      DIA(22) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA22,
      DIA(21) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA21,
      DIA(20) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA20,
      DIA(19) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA19,
      DIA(18) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA18,
      DIA(17) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA17,
      DIA(16) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA16,
      DIA(15) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA15,
      DIA(14) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA14,
      DIA(13) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA13,
      DIA(12) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA12,
      DIA(11) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA11,
      DIA(10) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA10,
      DIA(9) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA9,
      DIA(8) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA8,
      DIA(7) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA7,
      DIA(6) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA6,
      DIA(5) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA5,
      DIA(4) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA4,
      DIA(3) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA3,
      DIA(2) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA2,
      DIA(1) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA1,
      DIA(0) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA0,
      DIPA(3) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA3,
      DIPA(2) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA2,
      DIPA(1) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA1,
      DIPA(0) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA0,
      DIB(31) => '0',
      DIB(30) => '0',
      DIB(29) => '0',
      DIB(28) => '0',
      DIB(27) => '0',
      DIB(26) => '0',
      DIB(25) => '0',
      DIB(24) => '0',
      DIB(23) => '0',
      DIB(22) => '0',
      DIB(21) => '0',
      DIB(20) => '0',
      DIB(19) => '0',
      DIB(18) => '0',
      DIB(17) => '0',
      DIB(16) => '0',
      DIB(15) => '0',
      DIB(14) => '0',
      DIB(13) => '0',
      DIB(12) => '0',
      DIB(11) => '0',
      DIB(10) => '0',
      DIB(9) => '0',
      DIB(8) => '0',
      DIB(7) => '0',
      DIB(6) => '0',
      DIB(5) => '0',
      DIB(4) => '0',
      DIB(3) => '0',
      DIB(2) => '0',
      DIB(1) => '0',
      DIB(0) => '0',
      DIPB(3) => '0',
      DIPB(2) => '0',
      DIPB(1) => '0',
      DIPB(0) => '0',
      DOA(31) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA31,
      DOA(30) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA30,
      DOA(29) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA29,
      DOA(28) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA28,
      DOA(27) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA27,
      DOA(26) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA26,
      DOA(25) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA25,
      DOA(24) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA24,
      DOA(23) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA23,
      DOA(22) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA22,
      DOA(21) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA21,
      DOA(20) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA20,
      DOA(19) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA19,
      DOA(18) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA18,
      DOA(17) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA17,
      DOA(16) => i_dp_mem_l(1),
      DOA(15) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA15,
      DOA(14) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA14,
      DOA(13) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA13,
      DOA(12) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA12,
      DOA(11) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA11,
      DOA(10) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA10,
      DOA(9) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA9,
      DOA(8) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA8,
      DOA(7) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA7,
      DOA(6) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA6,
      DOA(5) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA5,
      DOA(4) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA4,
      DOA(3) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA3,
      DOA(2) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA2,
      DOA(1) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA1,
      DOA(0) => i_dp_mem_l(0),
      DOPA(3) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA3,
      DOPA(2) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA2,
      DOPA(1) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA1,
      DOPA(0) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA0,
      DOB(31) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB31,
      DOB(30) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB30,
      DOB(29) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB29,
      DOB(28) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB28,
      DOB(27) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB27,
      DOB(26) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB26,
      DOB(25) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB25,
      DOB(24) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB24,
      DOB(23) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB23,
      DOB(22) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB22,
      DOB(21) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB21,
      DOB(20) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB20,
      DOB(19) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB19,
      DOB(18) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB18,
      DOB(17) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB17,
      DOB(16) => i_dp_mem_l(3),
      DOB(15) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB15,
      DOB(14) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB14,
      DOB(13) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB13,
      DOB(12) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB12,
      DOB(11) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB11,
      DOB(10) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB10,
      DOB(9) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB9,
      DOB(8) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB8,
      DOB(7) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB7,
      DOB(6) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB6,
      DOB(5) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB5,
      DOB(4) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB4,
      DOB(3) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB3,
      DOB(2) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB2,
      DOB(1) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB1,
      DOB(0) => i_dp_mem_l(2),
      DOPB(3) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB3,
      DOPB(2) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB2,
      DOPB(1) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB1,
      DOPB(0) => i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB0
    );
  i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram : X_RAMB16_S36_S36
    generic map(
      INIT_A => X"000000000",
      INIT_B => X"000000000",
      SRVAL_A => X"000000000",
      SRVAL_B => X"000000000",
      SIM_COLLISION_CHECK => "ALL",
      WRITE_MODE_A => "WRITE_FIRST",
      WRITE_MODE_B => "WRITE_FIRST",
      INIT_00 => X"0000000000000001000000000000000000000001000000000000000100000001",
      INIT_01 => X"0001000000000001000000000001000000000001000100000000000100010001",
      INIT_02 => X"0000000000000000000000000000000000010000000000000000000000010001",
      INIT_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_08 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_09 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_0F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_10 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_11 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_12 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_13 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_14 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_15 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_16 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_17 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_18 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_19 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_1F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_20 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_21 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_22 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_23 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_24 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_25 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_26 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_27 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_28 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_29 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_2F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_30 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_31 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_32 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_33 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_34 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_35 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_36 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_37 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_38 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_39 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3A => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3B => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3C => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3D => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3E => X"0000000000000000000000000000000000000000000000000000000000000000",
      INIT_3F => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_00 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_01 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_02 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_03 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_04 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_05 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_06 => X"0000000000000000000000000000000000000000000000000000000000000000",
      INITP_07 => X"0000000000000000000000000000000000000000000000000000000000000000",
      LOC => "RAMB16_X1Y10",
      SETUP_ALL => 421 ps,
      SETUP_READ_FIRST => 421 ps
    )
    port map (
      CLKA => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA,
      CLKB => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB,
      ENA => spin_0,
      ENB => spin_0,
      SSRA => '0',
      SSRB => '0',
      WEA => '0',
      WEB => '0',
      ADDRA(8) => '0',
      ADDRA(7) => '0',
      ADDRA(6) => '0',
      ADDRA(5) => '0',
      ADDRA(4) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q,
      ADDRA(3) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q,
      ADDRA(2) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q,
      ADDRA(1) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q,
      ADDRA(0) => '0',
      ADDRB(8) => '0',
      ADDRB(7) => '0',
      ADDRB(6) => '0',
      ADDRB(5) => '0',
      ADDRB(4) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q,
      ADDRB(3) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q,
      ADDRB(2) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q,
      ADDRB(1) => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q,
      ADDRB(0) => '1',
      DIA(31) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA31,
      DIA(30) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA30,
      DIA(29) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA29,
      DIA(28) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA28,
      DIA(27) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA27,
      DIA(26) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA26,
      DIA(25) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA25,
      DIA(24) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA24,
      DIA(23) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA23,
      DIA(22) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA22,
      DIA(21) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA21,
      DIA(20) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA20,
      DIA(19) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA19,
      DIA(18) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA18,
      DIA(17) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA17,
      DIA(16) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA16,
      DIA(15) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA15,
      DIA(14) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA14,
      DIA(13) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA13,
      DIA(12) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA12,
      DIA(11) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA11,
      DIA(10) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA10,
      DIA(9) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA9,
      DIA(8) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA8,
      DIA(7) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA7,
      DIA(6) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA6,
      DIA(5) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA5,
      DIA(4) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA4,
      DIA(3) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA3,
      DIA(2) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA2,
      DIA(1) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA1,
      DIA(0) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIA0,
      DIPA(3) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA3,
      DIPA(2) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA2,
      DIPA(1) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA1,
      DIPA(0) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DIPA0,
      DIB(31) => '0',
      DIB(30) => '0',
      DIB(29) => '0',
      DIB(28) => '0',
      DIB(27) => '0',
      DIB(26) => '0',
      DIB(25) => '0',
      DIB(24) => '0',
      DIB(23) => '0',
      DIB(22) => '0',
      DIB(21) => '0',
      DIB(20) => '0',
      DIB(19) => '0',
      DIB(18) => '0',
      DIB(17) => '0',
      DIB(16) => '0',
      DIB(15) => '0',
      DIB(14) => '0',
      DIB(13) => '0',
      DIB(12) => '0',
      DIB(11) => '0',
      DIB(10) => '0',
      DIB(9) => '0',
      DIB(8) => '0',
      DIB(7) => '0',
      DIB(6) => '0',
      DIB(5) => '0',
      DIB(4) => '0',
      DIB(3) => '0',
      DIB(2) => '0',
      DIB(1) => '0',
      DIB(0) => '0',
      DIPB(3) => '0',
      DIPB(2) => '0',
      DIPB(1) => '0',
      DIPB(0) => '0',
      DOA(31) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA31,
      DOA(30) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA30,
      DOA(29) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA29,
      DOA(28) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA28,
      DOA(27) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA27,
      DOA(26) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA26,
      DOA(25) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA25,
      DOA(24) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA24,
      DOA(23) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA23,
      DOA(22) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA22,
      DOA(21) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA21,
      DOA(20) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA20,
      DOA(19) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA19,
      DOA(18) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA18,
      DOA(17) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA17,
      DOA(16) => i_dp_mem_r(1),
      DOA(15) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA15,
      DOA(14) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA14,
      DOA(13) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA13,
      DOA(12) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA12,
      DOA(11) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA11,
      DOA(10) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA10,
      DOA(9) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA9,
      DOA(8) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA8,
      DOA(7) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA7,
      DOA(6) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA6,
      DOA(5) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA5,
      DOA(4) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA4,
      DOA(3) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA3,
      DOA(2) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA2,
      DOA(1) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOA1,
      DOA(0) => i_dp_mem_r(0),
      DOPA(3) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA3,
      DOPA(2) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA2,
      DOPA(1) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA1,
      DOPA(0) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPA0,
      DOB(31) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB31,
      DOB(30) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB30,
      DOB(29) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB29,
      DOB(28) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB28,
      DOB(27) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB27,
      DOB(26) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB26,
      DOB(25) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB25,
      DOB(24) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB24,
      DOB(23) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB23,
      DOB(22) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB22,
      DOB(21) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB21,
      DOB(20) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB20,
      DOB(19) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB19,
      DOB(18) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB18,
      DOB(17) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB17,
      DOB(16) => i_dp_mem_r(3),
      DOB(15) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB15,
      DOB(14) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB14,
      DOB(13) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB13,
      DOB(12) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB12,
      DOB(11) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB11,
      DOB(10) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB10,
      DOB(9) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB9,
      DOB(8) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB8,
      DOB(7) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB7,
      DOB(6) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB6,
      DOB(5) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB5,
      DOB(4) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB4,
      DOB(3) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB3,
      DOB(2) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB2,
      DOB(1) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOB1,
      DOB(0) => i_dp_mem_r(2),
      DOPB(3) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB3,
      DOPB(2) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB2,
      DOPB(1) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB1,
      DOPB(0) => i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_DOPB0
    );
  leds_1_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X55Y83"
    )
    port map (
      IA => N129,
      IB => N130,
      SEL => leds_1_OBUF_BXINV_2107,
      O => leds_1_OBUF_F5MUX_2114
    );
  leds_1_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X55Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_1_OBUF_BXINV_2107
    );
  i_dp_mux12_F : X_LUT4
    generic map(
      INIT => X"00D8",
      LOC => "SLICE_X55Y83"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_dp_leds_win(0),
      ADR2 => i_dp_leds_wait(1),
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N129
    );
  i_dp_mux12_G : X_LUT4
    generic map(
      INIT => X"F0E4",
      LOC => "SLICE_X55Y83"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_dp_leds_wait(1),
      ADR2 => i_dp_leds_lose(1),
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N130
    );
  leds_0_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X54Y82"
    )
    port map (
      IA => N127,
      IB => N128,
      SEL => leds_0_OBUF_BXINV_2132,
      O => leds_0_OBUF_F5MUX_2139
    );
  leds_0_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X54Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_0_OBUF_BXINV_2132
    );
  i_dp_mux21_F : X_LUT4
    generic map(
      INIT => X"4450",
      LOC => "SLICE_X54Y82"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_dp_leds_win(0),
      ADR2 => i_dp_leds_wait(0),
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => N127
    );
  i_dp_mux21_G : X_LUT4
    generic map(
      INIT => X"FE10",
      LOC => "SLICE_X54Y82"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => i_dp_leds_wait(0),
      ADR3 => i_dp_leds_lose(0),
      O => N128
    );
  leds_2_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X54Y84"
    )
    port map (
      IA => N125,
      IB => N126,
      SEL => leds_2_OBUF_BXINV_2157,
      O => leds_2_OBUF_F5MUX_2164
    );
  leds_2_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X54Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_2_OBUF_BXINV_2157
    );
  i_dp_mux22_F : X_LUT4
    generic map(
      INIT => X"5044",
      LOC => "SLICE_X54Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_dp_leds_wait(2),
      ADR2 => i_dp_leds_win(0),
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => N125
    );
  i_dp_mux22_G : X_LUT4
    generic map(
      INIT => X"FE04",
      LOC => "SLICE_X54Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_dp_leds_wait(2),
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_dp_leds_lose(2),
      O => N126
    );
  leds_3_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X55Y85"
    )
    port map (
      IA => N123,
      IB => N124,
      SEL => leds_3_OBUF_BXINV_2182,
      O => leds_3_OBUF_F5MUX_2189
    );
  leds_3_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X55Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_3_OBUF_BXINV_2182
    );
  i_dp_mux32_F : X_LUT4
    generic map(
      INIT => X"00CA",
      LOC => "SLICE_X55Y85"
    )
    port map (
      ADR0 => i_dp_leds_wait(3),
      ADR1 => i_dp_leds_win(0),
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N123
    );
  i_dp_mux32_G : X_LUT4
    generic map(
      INIT => X"AAAC",
      LOC => "SLICE_X55Y85"
    )
    port map (
      ADR0 => i_dp_leds_lose(3),
      ADR1 => i_dp_leds_wait(3),
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N124
    );
  leds_4_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X53Y85"
    )
    port map (
      IA => N121,
      IB => N122,
      SEL => leds_4_OBUF_BXINV_2207,
      O => leds_4_OBUF_F5MUX_2214
    );
  leds_4_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X53Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_4_OBUF_BXINV_2207
    );
  i_dp_mux42_F : X_LUT4
    generic map(
      INIT => X"00CA",
      LOC => "SLICE_X53Y85"
    )
    port map (
      ADR0 => i_dp_leds_wait(4),
      ADR1 => i_dp_leds_win(0),
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N121
    );
  i_dp_mux42_G : X_LUT4
    generic map(
      INIT => X"AAAC",
      LOC => "SLICE_X53Y85"
    )
    port map (
      ADR0 => i_dp_leds_lose(4),
      ADR1 => i_dp_leds_wait(4),
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N122
    );
  leds_5_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X51Y85"
    )
    port map (
      IA => N119,
      IB => N120,
      SEL => leds_5_OBUF_BXINV_2232,
      O => leds_5_OBUF_F5MUX_2239
    );
  leds_5_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X51Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_5_OBUF_BXINV_2232
    );
  i_dp_mux52_F : X_LUT4
    generic map(
      INIT => X"00CA",
      LOC => "SLICE_X51Y85"
    )
    port map (
      ADR0 => i_dp_leds_wait(5),
      ADR1 => i_dp_leds_win(0),
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N119
    );
  i_dp_mux52_G : X_LUT4
    generic map(
      INIT => X"FE02",
      LOC => "SLICE_X51Y85"
    )
    port map (
      ADR0 => i_dp_leds_wait(5),
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_dp_leds_lose(5),
      O => N120
    );
  i_cu_current_state_FSM_FFd2_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X43Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd2_F5MUX_2268,
      O => i_cu_current_state_FSM_FFd2_DXMUX_2270
    );
  i_cu_current_state_FSM_FFd2_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X43Y84"
    )
    port map (
      IA => N131,
      IB => N132,
      SEL => i_cu_current_state_FSM_FFd2_BXINV_2261,
      O => i_cu_current_state_FSM_FFd2_F5MUX_2268
    );
  i_cu_current_state_FSM_FFd2_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X43Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd3_894,
      O => i_cu_current_state_FSM_FFd2_BXINV_2261
    );
  i_cu_current_state_FSM_FFd2_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X43Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_cu_current_state_FSM_FFd2_CLKINV_2252
    );
  i_cu_current_state_FSM_FFd2_In_F : X_LUT4
    generic map(
      INIT => X"0E0E",
      LOC => "SLICE_X43Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_cu_current_state_FSM_FFd1_917,
      ADR2 => seqfin_0,
      ADR3 => VCC,
      O => N131
    );
  leds_6_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X50Y84"
    )
    port map (
      IA => N117,
      IB => N118,
      SEL => leds_6_OBUF_BXINV_2293,
      O => leds_6_OBUF_F5MUX_2300
    );
  leds_6_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X50Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_6_OBUF_BXINV_2293
    );
  i_dp_mux62_F : X_LUT4
    generic map(
      INIT => X"5140",
      LOC => "SLICE_X50Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => i_dp_leds_win(0),
      ADR3 => i_dp_leds_wait(6),
      O => N117
    );
  i_dp_mux62_G : X_LUT4
    generic map(
      INIT => X"CDC8",
      LOC => "SLICE_X50Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_dp_leds_lose(6),
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => i_dp_leds_wait(6),
      O => N118
    );
  leds_7_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X48Y83"
    )
    port map (
      IA => N115,
      IB => N116,
      SEL => leds_7_OBUF_BXINV_2318,
      O => leds_7_OBUF_F5MUX_2325
    );
  leds_7_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X48Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_7_OBUF_BXINV_2318
    );
  i_dp_mux72_F : X_LUT4
    generic map(
      INIT => X"3120",
      LOC => "SLICE_X48Y83"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => i_dp_leds_win(0),
      ADR3 => i_dp_leds_wait(7),
      O => N115
    );
  i_dp_mux72_G : X_LUT4
    generic map(
      INIT => X"F1E0",
      LOC => "SLICE_X48Y83"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => i_dp_leds_lose(7),
      ADR3 => i_dp_leds_wait(7),
      O => N116
    );
  leds_8_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X49Y82"
    )
    port map (
      IA => N113,
      IB => N114,
      SEL => leds_8_OBUF_BXINV_2343,
      O => leds_8_OBUF_F5MUX_2350
    );
  leds_8_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X49Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_8_OBUF_BXINV_2343
    );
  i_dp_mux82_F : X_LUT4
    generic map(
      INIT => X"0D08",
      LOC => "SLICE_X49Y82"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_dp_leds_win(0),
      ADR2 => i_cu_current_state_FSM_FFd3_894,
      ADR3 => i_dp_leds_wait(8),
      O => N113
    );
  i_dp_mux82_G : X_LUT4
    generic map(
      INIT => X"FE04",
      LOC => "SLICE_X49Y82"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_dp_leds_wait(8),
      ADR2 => i_cu_current_state_FSM_FFd3_894,
      ADR3 => i_dp_leds_lose(8),
      O => N114
    );
  leds_9_OBUF_F5MUX : X_MUX2
    generic map(
      LOC => "SLICE_X51Y82"
    )
    port map (
      IA => N111,
      IB => N112,
      SEL => leds_9_OBUF_BXINV_2368,
      O => leds_9_OBUF_F5MUX_2375
    );
  leds_9_OBUF_BXINV : X_BUF
    generic map(
      LOC => "SLICE_X51Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_917,
      O => leds_9_OBUF_BXINV_2368
    );
  i_dp_mux92_F : X_LUT4
    generic map(
      INIT => X"00B8",
      LOC => "SLICE_X51Y82"
    )
    port map (
      ADR0 => i_dp_leds_win(0),
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => i_dp_leds_wait(9),
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N111
    );
  i_dp_mux92_G : X_LUT4
    generic map(
      INIT => X"AAB8",
      LOC => "SLICE_X51Y82"
    )
    port map (
      ADR0 => i_dp_leds_lose(9),
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => i_dp_leds_wait(9),
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => N112
    );
  i_divisor_d_clk_out_reg_cmp_eq0000_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X38Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_cmp_eq0000_SW0_O_pack_1,
      O => i_divisor_d_clk_out_reg_cmp_eq0000_SW0_O
    );
  i_divisor_d_clk_out_reg_cmp_eq0000_SW0 : X_LUT4
    generic map(
      INIT => X"3F3F",
      LOC => "SLICE_X38Y24"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_divisor_d_cntr_reg(4),
      ADR2 => i_divisor_d_cntr_reg(2),
      ADR3 => VCC,
      O => i_divisor_d_clk_out_reg_cmp_eq0000_SW0_O_pack_1
    );
  i_divisor_r_cntr_reg_0_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X49Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg,
      O => i_divisor_r_cntr_reg_0_DXMUX_2429
    );
  i_divisor_r_cntr_reg_0_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X49Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => N01_pack_2,
      O => N01
    );
  i_divisor_r_cntr_reg_0_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X49Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_r_cntr_reg_0_CLKINV_2412
    );
  i_divisor_r_Mcount_cntr_reg_xor_1_111 : X_LUT4
    generic map(
      INIT => X"FDFF",
      LOC => "SLICE_X49Y52"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(4),
      ADR1 => i_divisor_r_cntr_reg(3),
      ADR2 => i_divisor_r_cntr_reg(2),
      ADR3 => i_divisor_r_cntr_reg(5),
      O => N01_pack_2
    );
  i_divisor_l_cntr_reg_0_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X26Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg,
      O => i_divisor_l_cntr_reg_0_DXMUX_2464
    );
  i_divisor_l_cntr_reg_0_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X26Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg_xor_0_1_SW0_O_pack_2,
      O => i_divisor_l_Mcount_cntr_reg_xor_0_1_SW0_O
    );
  i_divisor_l_cntr_reg_0_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X26Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_l_cntr_reg_0_CLKINV_2447
    );
  i_divisor_l_Mcount_cntr_reg_xor_0_1_SW0 : X_LUT4
    generic map(
      INIT => X"0500",
      LOC => "SLICE_X26Y50"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(2),
      ADR1 => VCC,
      ADR2 => i_divisor_l_cntr_reg(1),
      ADR3 => i_divisor_l_cntr_reg(3),
      O => i_divisor_l_Mcount_cntr_reg_xor_0_1_SW0_O_pack_2
    );
  i_divisor_l_clk_out_reg_cmp_eq0000_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X28Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_clk_out_reg_cmp_eq0000_SW0_O_pack_1,
      O => i_divisor_l_clk_out_reg_cmp_eq0000_SW0_O
    );
  i_divisor_l_clk_out_reg_cmp_eq0000_SW0 : X_LUT4
    generic map(
      INIT => X"EEFF",
      LOC => "SLICE_X28Y51"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(1),
      ADR1 => i_divisor_l_cntr_reg(0),
      ADR2 => VCC,
      ADR3 => i_divisor_l_cntr_reg(3),
      O => i_divisor_l_clk_out_reg_cmp_eq0000_SW0_O_pack_1
    );
  i_divisor_r_cntr_reg_4_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X48Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg4_2520,
      O => i_divisor_r_cntr_reg_4_DXMUX_2523
    );
  i_divisor_r_cntr_reg_4_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X48Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg4_SW0_O_pack_2,
      O => i_divisor_r_Mcount_cntr_reg4_SW0_O
    );
  i_divisor_r_cntr_reg_4_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X48Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_r_cntr_reg_4_CLKINV_2507
    );
  i_divisor_r_Mcount_cntr_reg4_SW0 : X_LUT4
    generic map(
      INIT => X"0200",
      LOC => "SLICE_X48Y52"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(5),
      ADR1 => i_divisor_r_cntr_reg(0),
      ADR2 => i_divisor_r_cntr_reg(2),
      ADR3 => i_divisor_r_cntr_reg(1),
      O => i_divisor_r_Mcount_cntr_reg4_SW0_O_pack_2
    );
  i_divisor_l_cntr_reg_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X29Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg3,
      O => i_divisor_l_cntr_reg_3_DXMUX_2558
    );
  i_divisor_l_cntr_reg_3_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X29Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg_xor_3_1_SW1_O_pack_2,
      O => i_divisor_l_Mcount_cntr_reg_xor_3_1_SW1_O
    );
  i_divisor_l_cntr_reg_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X29Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_l_cntr_reg_3_CLKINV_2542
    );
  i_dp_leds_win_0 : X_FF
    generic map(
      LOC => "SLICE_X52Y80",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_win_0_DYMUX_1031,
      CE => VCC,
      CLK => i_dp_leds_win_0_CLKINV_1028,
      SET => GND,
      RST => i_dp_leds_win_0_FFY_RSTAND_1036,
      O => i_dp_leds_win(0)
    );
  i_dp_leds_win_0_FFY_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X52Y80",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_win_0_FFY_RSTAND_1036
    );
  i_divisor_l_Mcount_cntr_reg_xor_3_1_SW1 : X_LUT4
    generic map(
      INIT => X"9557",
      LOC => "SLICE_X29Y51"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(3),
      ADR1 => i_divisor_l_cntr_reg(2),
      ADR2 => i_divisor_l_cntr_reg(0),
      ADR3 => i_divisor_l_cntr_reg(1),
      O => i_divisor_l_Mcount_cntr_reg_xor_3_1_SW1_O_pack_2
    );
  i_divisor_l_cntr_reg_4_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X26Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg4,
      O => i_divisor_l_cntr_reg_4_DXMUX_2593
    );
  i_divisor_l_cntr_reg_4_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X26Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg_xor_4_1_SW0_O_pack_2,
      O => i_divisor_l_Mcount_cntr_reg_xor_4_1_SW0_O
    );
  i_divisor_l_cntr_reg_4_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X26Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_l_cntr_reg_4_CLKINV_2575
    );
  i_divisor_l_Mcount_cntr_reg_xor_4_1_SW0 : X_LUT4
    generic map(
      INIT => X"AA00",
      LOC => "SLICE_X26Y51"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(1),
      ADR1 => VCC,
      ADR2 => VCC,
      ADR3 => i_divisor_l_cntr_reg(3),
      O => i_divisor_l_Mcount_cntr_reg_xor_4_1_SW0_O_pack_2
    );
  i_divisor_r_clk_out_reg_cmp_eq0000_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X46Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_clk_out_reg_cmp_eq0000_SW0_O_pack_1,
      O => i_divisor_r_clk_out_reg_cmp_eq0000_SW0_O
    );
  i_divisor_r_clk_out_reg_cmp_eq0000_SW0 : X_LUT4
    generic map(
      INIT => X"FF3F",
      LOC => "SLICE_X46Y52"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_divisor_r_cntr_reg(4),
      ADR2 => i_divisor_r_cntr_reg(5),
      ADR3 => i_divisor_r_cntr_reg(3),
      O => i_divisor_r_clk_out_reg_cmp_eq0000_SW0_O_pack_1
    );
  i_cu_current_state_FSM_FFd1_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X53Y79",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_In,
      O => i_cu_current_state_FSM_FFd1_DXMUX_2652
    );
  i_cu_current_state_FSM_FFd1_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X53Y79",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_In96_SW0_O_pack_2,
      O => i_cu_current_state_FSM_FFd1_In96_SW0_O
    );
  i_cu_current_state_FSM_FFd1_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X53Y79",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_cu_current_state_FSM_FFd1_CLKINV_2635
    );
  i_cu_current_state_FSM_FFd1_In96_SW0 : X_LUT4
    generic map(
      INIT => X"FC00",
      LOC => "SLICE_X53Y79"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd1_In53_0,
      ADR2 => i_cu_current_state_FSM_FFd1_In26_0,
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => i_cu_current_state_FSM_FFd1_In96_SW0_O_pack_2
    );
  i_divisor_l_cntr_reg_5_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X28Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg5,
      O => i_divisor_l_cntr_reg_5_DXMUX_2687
    );
  i_divisor_l_cntr_reg_5_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X28Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg_xor_5_1_SW1_O_pack_2,
      O => i_divisor_l_Mcount_cntr_reg_xor_5_1_SW1_O
    );
  i_divisor_l_cntr_reg_5_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X28Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_l_cntr_reg_5_CLKINV_2671
    );
  i_divisor_l_Mcount_cntr_reg_xor_5_1_SW1 : X_LUT4
    generic map(
      INIT => X"0100",
      LOC => "SLICE_X28Y50"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(2),
      ADR1 => i_divisor_l_cntr_reg(0),
      ADR2 => i_divisor_l_cntr_reg(1),
      ADR3 => i_divisor_l_cntr_reg(3),
      O => i_divisor_l_Mcount_cntr_reg_xor_5_1_SW1_O_pack_2
    );
  i_divisor_d_cntr_reg_0_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X40Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_Mcount_cntr_reg,
      O => i_divisor_d_cntr_reg_0_DXMUX_2722
    );
  i_divisor_d_cntr_reg_0_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X40Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_Mcount_cntr_reg_xor_0_1_SW0_O_pack_2,
      O => i_divisor_d_Mcount_cntr_reg_xor_0_1_SW0_O
    );
  i_divisor_d_cntr_reg_0_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X40Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_d_cntr_reg_0_CLKINV_2704
    );
  i_divisor_d_Mcount_cntr_reg_xor_0_1_SW0 : X_LUT4
    generic map(
      INIT => X"C0C0",
      LOC => "SLICE_X40Y24"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_divisor_d_cntr_reg(4),
      ADR2 => i_divisor_d_cntr_reg(2),
      ADR3 => VCC,
      O => i_divisor_d_Mcount_cntr_reg_xor_0_1_SW0_O_pack_2
    );
  i_divisor_d_cntr_reg_2_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X41Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_Mcount_cntr_reg2,
      O => i_divisor_d_cntr_reg_2_DXMUX_2757
    );
  i_divisor_d_cntr_reg_2_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X41Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_Mcount_cntr_reg_xor_2_1_SW0_O_pack_2,
      O => i_divisor_d_Mcount_cntr_reg_xor_2_1_SW0_O
    );
  i_divisor_d_cntr_reg_2_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X41Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_d_cntr_reg_2_CLKINV_2739
    );
  i_divisor_d_Mcount_cntr_reg_xor_2_1_SW0 : X_LUT4
    generic map(
      INIT => X"AFAF",
      LOC => "SLICE_X41Y25"
    )
    port map (
      ADR0 => i_divisor_d_cntr_reg(3),
      ADR1 => VCC,
      ADR2 => i_divisor_d_cntr_reg(4),
      ADR3 => VCC,
      O => i_divisor_d_Mcount_cntr_reg_xor_2_1_SW0_O_pack_2
    );
  i_divisor_r_cntr_reg_5_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X48Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => N57,
      O => i_divisor_r_cntr_reg_5_DXMUX_2794
    );
  i_divisor_r_cntr_reg_5_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X48Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg5_SW0_SW0_O_pack_3,
      O => i_divisor_r_Mcount_cntr_reg5_SW0_SW0_O
    );
  i_divisor_r_cntr_reg_5_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X48Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_r_cntr_reg_5_CLKINV_2777
    );
  i_divisor_r_cntr_reg_5_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X48Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_cntr_reg(4),
      O => i_divisor_r_cntr_reg_5_CEINV_2776
    );
  i_divisor_d_cntr_reg_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X40Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_Mcount_cntr_reg3,
      O => i_divisor_d_cntr_reg_3_DXMUX_2837
    );
  i_divisor_d_cntr_reg_3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X40Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_Mcount_cntr_reg1,
      O => i_divisor_d_cntr_reg_3_DYMUX_2823
    );
  i_divisor_d_cntr_reg_3_SRINV : X_INV
    generic map(
      LOC => "SLICE_X40Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_d_cntr_reg_3_SRINVNOT
    );
  i_divisor_d_cntr_reg_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X40Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_d_cntr_reg_3_CLKINV_2812
    );
  i_divisor_d_Mcount_cntr_reg_xor_1_11 : X_LUT4
    generic map(
      INIT => X"6666",
      LOC => "SLICE_X40Y25"
    )
    port map (
      ADR0 => i_divisor_d_cntr_reg(1),
      ADR1 => i_divisor_d_cntr_reg(0),
      ADR2 => VCC,
      ADR3 => VCC,
      O => i_divisor_d_Mcount_cntr_reg1
    );
  i_dp_i_cnt_mod_r_cnt_in_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X45Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_Mcount_cnt_in3,
      O => i_dp_i_cnt_mod_r_cnt_in_3_DXMUX_2882
    );
  i_dp_i_cnt_mod_r_cnt_in_3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X45Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_Mcount_cnt_in2,
      O => i_dp_i_cnt_mod_r_cnt_in_3_DYMUX_2867
    );
  i_dp_i_cnt_mod_r_cnt_in_3_SRINV : X_INV
    generic map(
      LOC => "SLICE_X45Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_i_cnt_mod_r_cnt_in_3_SRINVNOT
    );
  i_dp_i_cnt_mod_r_cnt_in_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X45Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_clk_out_reg_921,
      O => i_dp_i_cnt_mod_r_cnt_in_3_CLKINV_2857
    );
  i_dp_i_cnt_mod_r_cnt_in_3_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X45Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => spin_0,
      O => i_dp_i_cnt_mod_r_cnt_in_3_CEINV_2856
    );
  i_dp_i_cnt_mod_r_Mcount_cnt_in_xor_2_11 : X_LUT4
    generic map(
      INIT => X"6A6A",
      LOC => "SLICE_X45Y89"
    )
    port map (
      ADR0 => i_dp_i_cnt_mod_r_cnt_in(2),
      ADR1 => i_dp_i_cnt_mod_r_cnt_in(1),
      ADR2 => i_dp_i_cnt_mod_r_cnt_in(0),
      ADR3 => VCC,
      O => i_dp_i_cnt_mod_r_Mcount_cnt_in2
    );
  i_divisor_d_cntr_reg_4_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X41Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_Mcount_cntr_reg_xor_4_1,
      O => i_divisor_d_cntr_reg_4_DYMUX_2908
    );
  i_divisor_d_cntr_reg_4_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X41Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_d_cntr_reg_4_CLKINV_2899
    );
  i_divisor_d_cntr_reg_4_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X41Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_cntr_reg(2),
      O => i_divisor_d_cntr_reg_4_CEINV_2898
    );
  i_divisor_l_cntr_reg_2_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X27Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg2,
      O => i_divisor_l_cntr_reg_2_DXMUX_2951
    );
  i_divisor_l_cntr_reg_2_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X27Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_Mcount_cntr_reg1,
      O => i_divisor_l_cntr_reg_2_DYMUX_2936
    );
  i_divisor_l_cntr_reg_2_SRINV : X_INV
    generic map(
      LOC => "SLICE_X27Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_l_cntr_reg_2_SRINVNOT
    );
  i_divisor_l_cntr_reg_2_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X27Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_l_cntr_reg_2_CLKINV_2925
    );
  i_divisor_l_Mcount_cntr_reg_xor_1_11 : X_LUT4
    generic map(
      INIT => X"33CC",
      LOC => "SLICE_X27Y50"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_divisor_l_cntr_reg(0),
      ADR2 => VCC,
      ADR3 => i_divisor_l_cntr_reg(1),
      O => i_divisor_l_Mcount_cntr_reg1
    );
  i_dp_i_cnt_mod_l_cnt_in_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X36Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_Mcount_cnt_in3,
      O => i_dp_i_cnt_mod_l_cnt_in_3_DXMUX_2996
    );
  i_dp_i_cnt_mod_l_cnt_in_3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X36Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_Mcount_cnt_in2,
      O => i_dp_i_cnt_mod_l_cnt_in_3_DYMUX_2981
    );
  i_dp_i_cnt_mod_l_cnt_in_3_SRINV : X_INV
    generic map(
      LOC => "SLICE_X36Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_i_cnt_mod_l_cnt_in_3_SRINVNOT
    );
  i_dp_i_cnt_mod_l_cnt_in_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X36Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_clk_out_reg_929,
      O => i_dp_i_cnt_mod_l_cnt_in_3_CLKINV_2971
    );
  i_dp_i_cnt_mod_l_cnt_in_3_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X36Y68",
      PATHPULSE => 526 ps
    )
    port map (
      I => spin_0,
      O => i_dp_i_cnt_mod_l_cnt_in_3_CEINV_2970
    );
  i_dp_i_cnt_mod_l_Mcount_cnt_in_xor_2_11 : X_LUT4
    generic map(
      INIT => X"5AF0",
      LOC => "SLICE_X36Y68"
    )
    port map (
      ADR0 => i_dp_i_cnt_mod_l_cnt_in(1),
      ADR1 => VCC,
      ADR2 => i_dp_i_cnt_mod_l_cnt_in(2),
      ADR3 => i_dp_i_cnt_mod_l_cnt_in(0),
      O => i_dp_i_cnt_mod_l_Mcount_cnt_in2
    );
  i_cu_current_state_FSM_FFd3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X42Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd3_In,
      O => i_cu_current_state_FSM_FFd3_DYMUX_3020
    );
  i_cu_current_state_FSM_FFd3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X42Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_cu_current_state_FSM_FFd3_CLKINV_3010
    );
  i_dp_i_cnt_seconds_cnt_in_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X42Y73",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_seconds_Mcount_cnt_in3,
      O => i_dp_i_cnt_seconds_cnt_in_3_DXMUX_3062
    );
  i_dp_i_cnt_seconds_cnt_in_3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X42Y73",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_seconds_Mcount_cnt_in2,
      O => i_dp_i_cnt_seconds_cnt_in_3_DYMUX_3048
    );
  i_dp_i_cnt_seconds_cnt_in_3_SRINV : X_BUF
    generic map(
      LOC => "SLICE_X42Y73",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_cntrst_inv_0,
      O => i_dp_i_cnt_seconds_cnt_in_3_SRINV_3039
    );
  i_dp_i_cnt_seconds_cnt_in_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X42Y73",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_i_cnt_seconds_cnt_in_3_CLKINV_3038
    );
  i_dp_i_cnt_seconds_Mcount_cnt_in_xor_2_11 : X_LUT4
    generic map(
      INIT => X"3CCC",
      LOC => "SLICE_X42Y73"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_dp_i_cnt_seconds_cnt_in(2),
      ADR2 => i_dp_i_cnt_seconds_cnt_in(0),
      ADR3 => i_dp_i_cnt_seconds_cnt_in(1),
      O => i_dp_i_cnt_seconds_Mcount_cnt_in2
    );
  i_divisor_r_cntr_reg_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X46Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg3,
      O => i_divisor_r_cntr_reg_3_DXMUX_3104
    );
  i_divisor_r_cntr_reg_3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X46Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_Mcount_cntr_reg2,
      O => i_divisor_r_cntr_reg_3_DYMUX_3090
    );
  i_divisor_r_cntr_reg_3_SRINV : X_INV
    generic map(
      LOC => "SLICE_X46Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_r_cntr_reg_3_SRINVNOT
    );
  i_divisor_r_cntr_reg_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X46Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_r_cntr_reg_3_CLKINV_3080
    );
  i_divisor_r_Mcount_cntr_reg_xor_2_11 : X_LUT4
    generic map(
      INIT => X"6A6A",
      LOC => "SLICE_X46Y53"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(2),
      ADR1 => i_divisor_r_cntr_reg(0),
      ADR2 => i_divisor_r_cntr_reg(1),
      ADR3 => VCC,
      O => i_divisor_r_Mcount_cntr_reg2
    );
  right_1_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X47Y94",
      PATHPULSE => 526 ps
    )
    port map (
      I => N93_pack_1,
      O => N93
    );
  i_dp_right_1_11_SW0 : X_LUT4
    generic map(
      INIT => X"D680",
      LOC => "SLICE_X47Y94"
    )
    port map (
      ADR0 => i_dp_mem_r(0),
      ADR1 => i_dp_mem_r(1),
      ADR2 => i_dp_mem_r(3),
      ADR3 => i_dp_mem_r(2),
      O => N93_pack_1
    );
  i_dp_i_cnt_mod_r_cnt_in_0_DXMUX : X_INV
    generic map(
      LOC => "SLICE_X44Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(0),
      O => i_dp_i_cnt_mod_r_cnt_in_0_DXMUX_3164
    );
  i_dp_i_cnt_mod_r_cnt_in_0_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X44Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_Mcount_cnt_in1,
      O => i_dp_i_cnt_mod_r_cnt_in_0_DYMUX_3155
    );
  i_dp_i_cnt_mod_r_cnt_in_0_SRINV : X_INV
    generic map(
      LOC => "SLICE_X44Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_i_cnt_mod_r_cnt_in_0_SRINVNOT
    );
  i_dp_i_cnt_mod_r_cnt_in_0_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X44Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_r_clk_out_reg_921,
      O => i_dp_i_cnt_mod_r_cnt_in_0_CLKINV_3146
    );
  i_dp_i_cnt_mod_r_cnt_in_0_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X44Y89",
      PATHPULSE => 526 ps
    )
    port map (
      I => spin_0,
      O => i_dp_i_cnt_mod_r_cnt_in_0_CEINV_3145
    );
  i_dp_leds_lose_1_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X54Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(2),
      O => i_dp_leds_lose_1_DXMUX_3189
    );
  i_dp_leds_lose_1_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X54Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(1),
      O => i_dp_leds_lose_1_DYMUX_3181
    );
  i_dp_leds_lose_1_SRINV : X_INV
    generic map(
      LOC => "SLICE_X54Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_lose_1_SRINVNOT
    );
  i_dp_leds_lose_1_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X54Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_lose_1_CLKINV_3178
    );
  right_5_OBUF_YUSED : X_BUF
    generic map(
      LOC => "SLICE_X43Y94",
      PATHPULSE => 526 ps
    )
    port map (
      I => N85_pack_1,
      O => N85
    );
  i_dp_right_5_11_SW0 : X_LUT4
    generic map(
      INIT => X"9FCD",
      LOC => "SLICE_X43Y94"
    )
    port map (
      ADR0 => i_dp_mem_r(1),
      ADR1 => i_dp_mem_r(3),
      ADR2 => i_dp_mem_r(0),
      ADR3 => i_dp_mem_r(2),
      O => N85_pack_1
    );
  i_dp_leds_lose_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X55Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(4),
      O => i_dp_leds_lose_3_DXMUX_3236
    );
  i_dp_leds_lose_3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X55Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(3),
      O => i_dp_leds_lose_3_DYMUX_3228
    );
  i_dp_leds_lose_3_SRINV : X_INV
    generic map(
      LOC => "SLICE_X55Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_lose_3_SRINVNOT
    );
  i_dp_leds_lose_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X55Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_lose_3_CLKINV_3225
    );
  i_dp_leds_lose_5_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X50Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(6),
      O => i_dp_leds_lose_5_DXMUX_3259
    );
  i_dp_leds_lose_5_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X50Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(5),
      O => i_dp_leds_lose_5_DYMUX_3251
    );
  i_dp_leds_lose_5_SRINV : X_INV
    generic map(
      LOC => "SLICE_X50Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_lose_5_SRINVNOT
    );
  i_dp_leds_lose_5_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X50Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_lose_5_CLKINV_3248
    );
  i_dp_leds_lose_7_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X49Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(8),
      O => i_dp_leds_lose_7_DXMUX_3282
    );
  i_dp_leds_lose_7_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X49Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(7),
      O => i_dp_leds_lose_7_DYMUX_3274
    );
  i_dp_leds_lose_7_SRINV : X_INV
    generic map(
      LOC => "SLICE_X49Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_lose_7_SRINVNOT
    );
  i_dp_leds_lose_7_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X49Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_lose_7_CLKINV_3271
    );
  i_dp_leds_wait_1_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X55Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(2),
      O => i_dp_leds_wait_1_DXMUX_3305
    );
  i_dp_leds_wait_1_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X55Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(1),
      O => i_dp_leds_wait_1_DYMUX_3297
    );
  i_dp_leds_wait_1_SRINV : X_INV
    generic map(
      LOC => "SLICE_X55Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_wait_1_SRINVNOT
    );
  i_dp_leds_wait_1_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X55Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_wait_1_CLKINV_3294
    );
  i_dp_leds_lose_9_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X50Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(0),
      O => i_dp_leds_lose_9_DXMUX_3329
    );
  i_dp_leds_lose_9_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X50Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_lose(9),
      O => i_dp_leds_lose_9_DYMUX_3321
    );
  i_dp_leds_lose_9_SRINV : X_INV
    generic map(
      LOC => "SLICE_X50Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_lose_9_SRINVNOT
    );
  i_dp_leds_lose_9_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X50Y82",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_lose_9_CLKINV_3318
    );
  i_dp_leds_wait_3_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X52Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(4),
      O => i_dp_leds_wait_3_DXMUX_3352
    );
  i_dp_leds_wait_3_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X52Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(3),
      O => i_dp_leds_wait_3_DYMUX_3344
    );
  i_dp_leds_wait_3_SRINV : X_INV
    generic map(
      LOC => "SLICE_X52Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_wait_3_SRINVNOT
    );
  i_dp_leds_wait_3_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X52Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_wait_3_CLKINV_3341
    );
  i_dp_leds_wait_5_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X50Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(6),
      O => i_dp_leds_wait_5_DXMUX_3376
    );
  i_dp_leds_wait_5_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X50Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(5),
      O => i_dp_leds_wait_5_DYMUX_3368
    );
  i_dp_leds_wait_5_SRINV : X_INV
    generic map(
      LOC => "SLICE_X50Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_wait_5_SRINVNOT
    );
  i_dp_leds_wait_5_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X50Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_wait_5_CLKINV_3365
    );
  i_divisor_l_clk_out_reg_DYMUX : X_INV
    generic map(
      LOC => "SLICE_X30Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_clk_out_reg_929,
      O => i_divisor_l_clk_out_reg_DYMUX_3392
    );
  i_divisor_l_clk_out_reg_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X30Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => i_divisor_l_clk_out_reg_CLKINV_3389
    );
  i_divisor_l_clk_out_reg_CEINV : X_BUF
    generic map(
      LOC => "SLICE_X30Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_l_clk_out_reg_cmp_eq0000_2492,
      O => i_divisor_l_clk_out_reg_CEINV_3388
    );
  i_cu_current_state_FSM_FFd1_In26_XUSED : X_BUF
    generic map(
      LOC => "SLICE_X52Y79",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_In26_3409,
      O => i_cu_current_state_FSM_FFd1_In26_0
    );
  i_cu_current_state_FSM_FFd1_In26 : X_LUT4
    generic map(
      INIT => X"7DBE",
      LOC => "SLICE_X52Y79"
    )
    port map (
      ADR0 => i_dp_mem_r(0),
      ADR1 => i_dp_mem_r(1),
      ADR2 => i_dp_mem_l(1),
      ADR3 => i_dp_mem_l(0),
      O => i_cu_current_state_FSM_FFd1_In26_3409
    );
  i_dp_leds_wait_7_DXMUX : X_BUF
    generic map(
      LOC => "SLICE_X51Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(8),
      O => i_dp_leds_wait_7_DXMUX_3429
    );
  i_dp_leds_wait_7_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X51Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(7),
      O => i_dp_leds_wait_7_DYMUX_3421
    );
  i_dp_leds_wait_7_SRINV : X_INV
    generic map(
      LOC => "SLICE_X51Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_wait_7_SRINVNOT
    );
  i_dp_leds_wait_7_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X51Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_wait_7_CLKINV_3418
    );
  i_dp_leds_wait_9_DXMUX : X_INV
    generic map(
      LOC => "SLICE_X52Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(0),
      O => i_dp_leds_wait_9_DXMUX_3453
    );
  i_dp_leds_wait_9_DYMUX : X_BUF
    generic map(
      LOC => "SLICE_X52Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_leds_wait(9),
      O => i_dp_leds_wait_9_DYMUX_3445
    );
  i_dp_leds_wait_9_SRINV : X_INV
    generic map(
      LOC => "SLICE_X52Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_dp_leds_wait_9_SRINVNOT
    );
  i_dp_leds_wait_9_CLKINV : X_BUF
    generic map(
      LOC => "SLICE_X52Y83",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_divisor_d_clk_out_reg_891,
      O => i_dp_leds_wait_9_CLKINV_3442
    );
  i_cu_current_state_FSM_FFd1_In53_XUSED : X_BUF
    generic map(
      LOC => "SLICE_X53Y78",
      PATHPULSE => 526 ps
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_In53_3469,
      O => i_cu_current_state_FSM_FFd1_In53_0
    );
  i_cu_current_state_FSM_FFd1_In53 : X_LUT4
    generic map(
      INIT => X"7BDE",
      LOC => "SLICE_X53Y78"
    )
    port map (
      ADR0 => i_dp_mem_r(3),
      ADR1 => i_dp_mem_r(2),
      ADR2 => i_dp_mem_l(3),
      ADR3 => i_dp_mem_l(2),
      O => i_cu_current_state_FSM_FFd1_In53_3469
    );
  i_dp_i_cnt_mod_l_cnt_in_0 : X_FF
    generic map(
      LOC => "SLICE_X37Y68",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in_0_DXMUX_1321,
      CE => i_dp_i_cnt_mod_l_cnt_in_0_CEINV_1302,
      CLK => i_dp_i_cnt_mod_l_cnt_in_0_CLKINV_1303,
      SET => GND,
      RST => i_dp_i_cnt_mod_l_cnt_in_0_SRINVNOT,
      O => i_dp_i_cnt_mod_l_cnt_in(0)
    );
  i_dp_left_5_11 : X_LUT4
    generic map(
      INIT => X"E0E0",
      LOC => "SLICE_X35Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => N99,
      ADR3 => VCC,
      O => left_5_OBUF_1350
    );
  i_dp_right_0_11 : X_LUT4
    generic map(
      INIT => X"E0E0",
      LOC => "SLICE_X46Y95"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => N95,
      ADR3 => VCC,
      O => right_0_OBUF_1374
    );
  i_divisor_d_clk_out_reg : X_FF
    generic map(
      LOC => "SLICE_X38Y25",
      INIT => '0'
    )
    port map (
      I => i_divisor_d_clk_out_reg1_DYMUX_1386,
      CE => i_divisor_d_clk_out_reg1_CEINV_1382,
      CLK => i_divisor_d_clk_out_reg1_CLKINV_1383,
      SET => GND,
      RST => i_divisor_d_clk_out_reg1_FFY_RSTAND_1392,
      O => i_divisor_d_clk_out_reg1
    );
  i_divisor_d_clk_out_reg1_FFY_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X38Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_d_clk_out_reg1_FFY_RSTAND_1392
    );
  i_divisor_l_Mcount_cntr_reg_xor_5_1_SW0 : X_LUT4
    generic map(
      INIT => X"8000",
      LOC => "SLICE_X29Y50"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(3),
      ADR1 => i_divisor_l_cntr_reg(2),
      ADR2 => i_divisor_l_cntr_reg(0),
      ADR3 => i_divisor_l_cntr_reg(1),
      O => N54
    );
  i_dp_right_4_11 : X_LUT4
    generic map(
      INIT => X"FA00",
      LOC => "SLICE_X44Y94"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => VCC,
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => N87,
      O => right_4_OBUF_1439
    );
  i_dp_left_2_11 : X_LUT4
    generic map(
      INIT => X"0E0E",
      LOC => "SLICE_X37Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => N105,
      ADR3 => VCC,
      O => left_2_OBUF_1463
    );
  i_dp_right_2_11 : X_LUT4
    generic map(
      INIT => X"00EE",
      LOC => "SLICE_X45Y95"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => VCC,
      ADR3 => N91,
      O => right_2_OBUF_1059
    );
  i_dp_i_cnt_seconds_cnt_in_0 : X_FF
    generic map(
      LOC => "SLICE_X43Y72",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_seconds_cnt_in_1_DYMUX_1086,
      CE => VCC,
      CLK => i_dp_i_cnt_seconds_cnt_in_1_CLKINV_1076,
      SET => GND,
      RST => i_dp_i_cnt_seconds_cnt_in_1_SRINV_1077,
      O => i_dp_i_cnt_seconds_cnt_in(0)
    );
  i_dp_i_cnt_seconds_Mcount_cnt_in_xor_1_11 : X_LUT4
    generic map(
      INIT => X"6266",
      LOC => "SLICE_X43Y72"
    )
    port map (
      ADR0 => i_dp_i_cnt_seconds_cnt_in(1),
      ADR1 => i_dp_i_cnt_seconds_cnt_in(0),
      ADR2 => i_dp_i_cnt_seconds_cnt_in(2),
      ADR3 => i_dp_i_cnt_seconds_cnt_in(3),
      O => i_dp_i_cnt_seconds_Mcount_cnt_in1
    );
  i_dp_i_cnt_seconds_cnt_in_1 : X_FF
    generic map(
      LOC => "SLICE_X43Y72",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_seconds_cnt_in_1_DXMUX_1100,
      CE => VCC,
      CLK => i_dp_i_cnt_seconds_cnt_in_1_CLKINV_1076,
      SET => GND,
      RST => i_dp_i_cnt_seconds_cnt_in_1_SRINV_1077,
      O => i_dp_i_cnt_seconds_cnt_in(1)
    );
  i_dp_left_0_11 : X_LUT4
    generic map(
      INIT => X"F0C0",
      LOC => "SLICE_X38Y85"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => N109,
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => left_0_OBUF_1128
    );
  i_dp_right_6_11 : X_LUT4
    generic map(
      INIT => X"FA00",
      LOC => "SLICE_X42Y94"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd3_894,
      ADR1 => VCC,
      ADR2 => i_cu_current_state_FSM_FFd2_895,
      ADR3 => N83,
      O => right_6_OBUF_1152
    );
  i_dp_left_4_11 : X_LUT4
    generic map(
      INIT => X"FA00",
      LOC => "SLICE_X37Y85"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => VCC,
      ADR2 => i_cu_current_state_FSM_FFd3_894,
      ADR3 => N101,
      O => left_4_OBUF_1176
    );
  i_cu_current_state_FSM_Out01 : X_LUT4
    generic map(
      INIT => X"00CC",
      LOC => "SLICE_X42Y84"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => VCC,
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => spin
    );
  i_dp_left_3_21 : X_LUT4
    generic map(
      INIT => X"0F0C",
      LOC => "SLICE_X36Y85"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => N103,
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => left_3_OBUF_1224
    );
  i_divisor_r_clk_out_reg : X_FF
    generic map(
      LOC => "SLICE_X44Y58",
      INIT => '0'
    )
    port map (
      I => i_divisor_r_clk_out_reg_DYMUX_1236,
      CE => i_divisor_r_clk_out_reg_CEINV_1232,
      CLK => i_divisor_r_clk_out_reg_CLKINV_1233,
      SET => GND,
      RST => i_divisor_r_clk_out_reg_FFY_RSTAND_1242,
      O => i_divisor_r_clk_out_reg_921
    );
  i_divisor_r_clk_out_reg_FFY_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X44Y58",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_r_clk_out_reg_FFY_RSTAND_1242
    );
  i_dp_right_3_11 : X_LUT4
    generic map(
      INIT => X"0F0A",
      LOC => "SLICE_X45Y94"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => VCC,
      ADR2 => N89,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => right_3_OBUF_1265
    );
  i_dp_left_1_11 : X_LUT4
    generic map(
      INIT => X"00FA",
      LOC => "SLICE_X39Y85"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => VCC,
      ADR2 => i_cu_current_state_FSM_FFd3_894,
      ADR3 => N107,
      O => left_1_OBUF_1289
    );
  i_dp_i_cnt_mod_l_Mcount_cnt_in_xor_1_11 : X_LUT4
    generic map(
      INIT => X"558A",
      LOC => "SLICE_X37Y68"
    )
    port map (
      ADR0 => i_dp_i_cnt_mod_l_cnt_in(0),
      ADR1 => i_dp_i_cnt_mod_l_cnt_in(2),
      ADR2 => i_dp_i_cnt_mod_l_cnt_in(3),
      ADR3 => i_dp_i_cnt_mod_l_cnt_in(1),
      O => i_dp_i_cnt_mod_l_Mcount_cnt_in1
    );
  i_dp_i_cnt_mod_l_cnt_in_1 : X_FF
    generic map(
      LOC => "SLICE_X37Y68",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in_0_DYMUX_1312,
      CE => i_dp_i_cnt_mod_l_cnt_in_0_CEINV_1302,
      CLK => i_dp_i_cnt_mod_l_cnt_in_0_CLKINV_1303,
      SET => GND,
      RST => i_dp_i_cnt_mod_l_cnt_in_0_SRINVNOT,
      O => i_dp_i_cnt_mod_l_cnt_in(1)
    );
  i_divisor_r_Mcount_cntr_reg_xor_1_12 : X_LUT4
    generic map(
      INIT => X"55A0",
      LOC => "SLICE_X49Y53"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(1),
      ADR1 => VCC,
      ADR2 => N01,
      ADR3 => i_divisor_r_cntr_reg(0),
      O => i_divisor_r_Mcount_cntr_reg1
    );
  i_divisor_r_cntr_reg_1 : X_FF
    generic map(
      LOC => "SLICE_X49Y53",
      INIT => '0'
    )
    port map (
      I => i_divisor_r_cntr_reg_1_DXMUX_1494,
      CE => VCC,
      CLK => i_divisor_r_cntr_reg_1_CLKINV_1476,
      SET => GND,
      RST => i_divisor_r_cntr_reg_1_FFX_RSTAND_1499,
      O => i_divisor_r_cntr_reg(1)
    );
  i_divisor_r_cntr_reg_1_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X49Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_r_cntr_reg_1_FFX_RSTAND_1499
    );
  i_dp_left_6_11 : X_LUT4
    generic map(
      INIT => X"EE00",
      LOC => "SLICE_X35Y85"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => VCC,
      ADR3 => N97,
      O => left_6_OBUF_1522
    );
  i_cu_current_state_FSM_FFd2_In_G : X_LUT4
    generic map(
      INIT => X"FFAE",
      LOC => "SLICE_X43Y84"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd2_895,
      ADR1 => i_cu_current_state_FSM_FFd1_917,
      ADR2 => seqfin_0,
      ADR3 => endb_IBUF_958,
      O => N132
    );
  i_cu_current_state_FSM_FFd2 : X_FF
    generic map(
      LOC => "SLICE_X43Y84",
      INIT => '0'
    )
    port map (
      I => i_cu_current_state_FSM_FFd2_DXMUX_2270,
      CE => VCC,
      CLK => i_cu_current_state_FSM_FFd2_CLKINV_2252,
      SET => GND,
      RST => i_cu_current_state_FSM_FFd2_FFX_RSTAND_2275,
      O => i_cu_current_state_FSM_FFd2_895
    );
  i_cu_current_state_FSM_FFd2_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X43Y84",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_cu_current_state_FSM_FFd2_FFX_RSTAND_2275
    );
  i_divisor_d_clk_out_reg_cmp_eq0000 : X_LUT4
    generic map(
      INIT => X"0001",
      LOC => "SLICE_X38Y24"
    )
    port map (
      ADR0 => i_divisor_d_cntr_reg(1),
      ADR1 => i_divisor_d_cntr_reg(3),
      ADR2 => i_divisor_d_cntr_reg(0),
      ADR3 => i_divisor_d_clk_out_reg_cmp_eq0000_SW0_O,
      O => i_divisor_d_clk_out_reg_cmp_eq0000_2398
    );
  i_divisor_r_Mcount_cntr_reg_xor_0_11 : X_LUT4
    generic map(
      INIT => X"00F5",
      LOC => "SLICE_X49Y52"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(1),
      ADR1 => VCC,
      ADR2 => N01,
      ADR3 => i_divisor_r_cntr_reg(0),
      O => i_divisor_r_Mcount_cntr_reg
    );
  i_divisor_r_cntr_reg_0 : X_FF
    generic map(
      LOC => "SLICE_X49Y52",
      INIT => '0'
    )
    port map (
      I => i_divisor_r_cntr_reg_0_DXMUX_2429,
      CE => VCC,
      CLK => i_divisor_r_cntr_reg_0_CLKINV_2412,
      SET => GND,
      RST => i_divisor_r_cntr_reg_0_FFX_RSTAND_2434,
      O => i_divisor_r_cntr_reg(0)
    );
  i_divisor_r_cntr_reg_0_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X49Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_r_cntr_reg_0_FFX_RSTAND_2434
    );
  i_divisor_l_Mcount_cntr_reg_xor_0_1 : X_LUT4
    generic map(
      INIT => X"0B0F",
      LOC => "SLICE_X26Y50"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(4),
      ADR1 => i_divisor_l_cntr_reg(5),
      ADR2 => i_divisor_l_cntr_reg(0),
      ADR3 => i_divisor_l_Mcount_cntr_reg_xor_0_1_SW0_O,
      O => i_divisor_l_Mcount_cntr_reg
    );
  i_divisor_l_cntr_reg_0 : X_FF
    generic map(
      LOC => "SLICE_X26Y50",
      INIT => '0'
    )
    port map (
      I => i_divisor_l_cntr_reg_0_DXMUX_2464,
      CE => VCC,
      CLK => i_divisor_l_cntr_reg_0_CLKINV_2447,
      SET => GND,
      RST => i_divisor_l_cntr_reg_0_FFX_RSTAND_2469,
      O => i_divisor_l_cntr_reg(0)
    );
  i_divisor_l_cntr_reg_0_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X26Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_l_cntr_reg_0_FFX_RSTAND_2469
    );
  i_divisor_l_clk_out_reg_cmp_eq0000 : X_LUT4
    generic map(
      INIT => X"0004",
      LOC => "SLICE_X28Y51"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(2),
      ADR1 => i_divisor_l_cntr_reg(5),
      ADR2 => i_divisor_l_clk_out_reg_cmp_eq0000_SW0_O,
      ADR3 => i_divisor_l_cntr_reg(4),
      O => i_divisor_l_clk_out_reg_cmp_eq0000_2492
    );
  i_divisor_r_Mcount_cntr_reg4 : X_LUT4
    generic map(
      INIT => X"909C",
      LOC => "SLICE_X48Y52"
    )
    port map (
      ADR0 => i_divisor_r_Mcount_cntr_reg3_bdd0_0,
      ADR1 => i_divisor_r_cntr_reg(4),
      ADR2 => i_divisor_r_cntr_reg(3),
      ADR3 => i_divisor_r_Mcount_cntr_reg4_SW0_O,
      O => i_divisor_r_Mcount_cntr_reg4_2520
    );
  i_divisor_r_cntr_reg_4 : X_FF
    generic map(
      LOC => "SLICE_X48Y52",
      INIT => '0'
    )
    port map (
      I => i_divisor_r_cntr_reg_4_DXMUX_2523,
      CE => VCC,
      CLK => i_divisor_r_cntr_reg_4_CLKINV_2507,
      SET => GND,
      RST => i_divisor_r_cntr_reg_4_FFX_RSTAND_2528,
      O => i_divisor_r_cntr_reg(4)
    );
  i_divisor_r_cntr_reg_4_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X48Y52",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_r_cntr_reg_4_FFX_RSTAND_2528
    );
  i_divisor_l_Mcount_cntr_reg_xor_3_1 : X_LUT4
    generic map(
      INIT => X"0D2F",
      LOC => "SLICE_X29Y51"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(5),
      ADR1 => i_divisor_l_cntr_reg(4),
      ADR2 => N51_0,
      ADR3 => i_divisor_l_Mcount_cntr_reg_xor_3_1_SW1_O,
      O => i_divisor_l_Mcount_cntr_reg3
    );
  i_divisor_l_cntr_reg_3 : X_FF
    generic map(
      LOC => "SLICE_X29Y51",
      INIT => '0'
    )
    port map (
      I => i_divisor_l_cntr_reg_3_DXMUX_2558,
      CE => VCC,
      CLK => i_divisor_l_cntr_reg_3_CLKINV_2542,
      SET => GND,
      RST => i_divisor_l_cntr_reg_3_FFX_RSTAND_2563,
      O => i_divisor_l_cntr_reg(3)
    );
  i_divisor_l_cntr_reg_3_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X29Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_l_cntr_reg_3_FFX_RSTAND_2563
    );
  i_divisor_l_Mcount_cntr_reg_xor_4_1 : X_LUT4
    generic map(
      INIT => X"7F80",
      LOC => "SLICE_X26Y51"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(2),
      ADR1 => i_divisor_l_cntr_reg(0),
      ADR2 => i_divisor_l_Mcount_cntr_reg_xor_4_1_SW0_O,
      ADR3 => i_divisor_l_cntr_reg(4),
      O => i_divisor_l_Mcount_cntr_reg4
    );
  i_divisor_l_cntr_reg_4 : X_FF
    generic map(
      LOC => "SLICE_X26Y51",
      INIT => '0'
    )
    port map (
      I => i_divisor_l_cntr_reg_4_DXMUX_2593,
      CE => VCC,
      CLK => i_divisor_l_cntr_reg_4_CLKINV_2575,
      SET => GND,
      RST => i_divisor_l_cntr_reg_4_FFX_RSTAND_2598,
      O => i_divisor_l_cntr_reg(4)
    );
  i_divisor_l_cntr_reg_4_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X26Y51",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_l_cntr_reg_4_FFX_RSTAND_2598
    );
  i_dp_leds_wait_1 : X_FF
    generic map(
      LOC => "SLICE_X55Y82",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_1_DXMUX_3305,
      CE => VCC,
      CLK => i_dp_leds_wait_1_CLKINV_3294,
      SET => GND,
      RST => i_dp_leds_wait_1_SRINVNOT,
      O => i_dp_leds_wait(1)
    );
  i_dp_leds_lose_8 : X_FF
    generic map(
      LOC => "SLICE_X50Y82",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_9_DYMUX_3321,
      CE => VCC,
      CLK => i_dp_leds_lose_9_CLKINV_3318,
      SET => GND,
      RST => i_dp_leds_lose_9_SRINVNOT,
      O => i_dp_leds_lose(8)
    );
  i_dp_leds_lose_9 : X_FF
    generic map(
      LOC => "SLICE_X50Y82",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_9_DXMUX_3329,
      CE => VCC,
      CLK => i_dp_leds_lose_9_CLKINV_3318,
      SET => i_dp_leds_lose_9_SRINVNOT,
      RST => GND,
      O => i_dp_leds_lose(9)
    );
  i_dp_leds_wait_2 : X_FF
    generic map(
      LOC => "SLICE_X52Y84",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_3_DYMUX_3344,
      CE => VCC,
      CLK => i_dp_leds_wait_3_CLKINV_3341,
      SET => GND,
      RST => i_dp_leds_wait_3_SRINVNOT,
      O => i_dp_leds_wait(2)
    );
  i_dp_leds_wait_3 : X_FF
    generic map(
      LOC => "SLICE_X52Y84",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_3_DXMUX_3352,
      CE => VCC,
      CLK => i_dp_leds_wait_3_CLKINV_3341,
      SET => GND,
      RST => i_dp_leds_wait_3_SRINVNOT,
      O => i_dp_leds_wait(3)
    );
  i_dp_leds_wait_4 : X_FF
    generic map(
      LOC => "SLICE_X50Y85",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_5_DYMUX_3368,
      CE => VCC,
      CLK => i_dp_leds_wait_5_CLKINV_3365,
      SET => GND,
      RST => i_dp_leds_wait_5_SRINVNOT,
      O => i_dp_leds_wait(4)
    );
  i_dp_leds_wait_5 : X_FF
    generic map(
      LOC => "SLICE_X50Y85",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_5_DXMUX_3376,
      CE => VCC,
      CLK => i_dp_leds_wait_5_CLKINV_3365,
      SET => GND,
      RST => i_dp_leds_wait_5_SRINVNOT,
      O => i_dp_leds_wait(5)
    );
  i_dp_i_cnt_mod_r_Mcount_cnt_in_xor_3_11 : X_LUT4
    generic map(
      INIT => X"6F80",
      LOC => "SLICE_X45Y89"
    )
    port map (
      ADR0 => i_dp_i_cnt_mod_r_cnt_in(2),
      ADR1 => i_dp_i_cnt_mod_r_cnt_in(1),
      ADR2 => i_dp_i_cnt_mod_r_cnt_in(0),
      ADR3 => i_dp_i_cnt_mod_r_cnt_in(3),
      O => i_dp_i_cnt_mod_r_Mcount_cnt_in3
    );
  i_dp_i_cnt_mod_r_cnt_in_3 : X_FF
    generic map(
      LOC => "SLICE_X45Y89",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in_3_DXMUX_2882,
      CE => i_dp_i_cnt_mod_r_cnt_in_3_CEINV_2856,
      CLK => i_dp_i_cnt_mod_r_cnt_in_3_CLKINV_2857,
      SET => GND,
      RST => i_dp_i_cnt_mod_r_cnt_in_3_SRINVNOT,
      O => i_dp_i_cnt_mod_r_cnt_in(3)
    );
  i_divisor_d_Mcount_cntr_reg_xor_4_11 : X_LUT4
    generic map(
      INIT => X"78E0",
      LOC => "SLICE_X41Y24"
    )
    port map (
      ADR0 => i_divisor_d_cntr_reg(3),
      ADR1 => i_divisor_d_cntr_reg(0),
      ADR2 => i_divisor_d_cntr_reg(4),
      ADR3 => i_divisor_d_cntr_reg(1),
      O => i_divisor_d_Mcount_cntr_reg_xor_4_1
    );
  i_divisor_d_cntr_reg_4 : X_FF
    generic map(
      LOC => "SLICE_X41Y24",
      INIT => '0'
    )
    port map (
      I => i_divisor_d_cntr_reg_4_DYMUX_2908,
      CE => i_divisor_d_cntr_reg_4_CEINV_2898,
      CLK => i_divisor_d_cntr_reg_4_CLKINV_2899,
      SET => GND,
      RST => i_divisor_d_cntr_reg_4_FFY_RSTAND_2914,
      O => i_divisor_d_cntr_reg(4)
    );
  i_divisor_d_cntr_reg_4_FFY_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X41Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_d_cntr_reg_4_FFY_RSTAND_2914
    );
  i_divisor_l_cntr_reg_1 : X_FF
    generic map(
      LOC => "SLICE_X27Y50",
      INIT => '0'
    )
    port map (
      I => i_divisor_l_cntr_reg_2_DYMUX_2936,
      CE => VCC,
      CLK => i_divisor_l_cntr_reg_2_CLKINV_2925,
      SET => GND,
      RST => i_divisor_l_cntr_reg_2_SRINVNOT,
      O => i_divisor_l_cntr_reg(1)
    );
  i_divisor_l_Mcount_cntr_reg_xor_2_11 : X_LUT4
    generic map(
      INIT => X"3CF0",
      LOC => "SLICE_X27Y50"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_divisor_l_cntr_reg(0),
      ADR2 => i_divisor_l_cntr_reg(2),
      ADR3 => i_divisor_l_cntr_reg(1),
      O => i_divisor_l_Mcount_cntr_reg2
    );
  i_divisor_l_cntr_reg_2 : X_FF
    generic map(
      LOC => "SLICE_X27Y50",
      INIT => '0'
    )
    port map (
      I => i_divisor_l_cntr_reg_2_DXMUX_2951,
      CE => VCC,
      CLK => i_divisor_l_cntr_reg_2_CLKINV_2925,
      SET => GND,
      RST => i_divisor_l_cntr_reg_2_SRINVNOT,
      O => i_divisor_l_cntr_reg(2)
    );
  i_dp_i_cnt_mod_l_cnt_in_2 : X_FF
    generic map(
      LOC => "SLICE_X36Y68",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in_3_DYMUX_2981,
      CE => i_dp_i_cnt_mod_l_cnt_in_3_CEINV_2970,
      CLK => i_dp_i_cnt_mod_l_cnt_in_3_CLKINV_2971,
      SET => GND,
      RST => i_dp_i_cnt_mod_l_cnt_in_3_SRINVNOT,
      O => i_dp_i_cnt_mod_l_cnt_in(2)
    );
  i_divisor_d_Mcount_cntr_reg_xor_2_1 : X_LUT4
    generic map(
      INIT => X"6C68",
      LOC => "SLICE_X41Y25"
    )
    port map (
      ADR0 => i_divisor_d_cntr_reg(0),
      ADR1 => i_divisor_d_cntr_reg(2),
      ADR2 => i_divisor_d_cntr_reg(1),
      ADR3 => i_divisor_d_Mcount_cntr_reg_xor_2_1_SW0_O,
      O => i_divisor_d_Mcount_cntr_reg2
    );
  i_divisor_d_cntr_reg_2 : X_FF
    generic map(
      LOC => "SLICE_X41Y25",
      INIT => '0'
    )
    port map (
      I => i_divisor_d_cntr_reg_2_DXMUX_2757,
      CE => VCC,
      CLK => i_divisor_d_cntr_reg_2_CLKINV_2739,
      SET => GND,
      RST => i_divisor_d_cntr_reg_2_FFX_RSTAND_2762,
      O => i_divisor_d_cntr_reg(2)
    );
  i_divisor_d_cntr_reg_2_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X41Y25",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_d_cntr_reg_2_FFX_RSTAND_2762
    );
  i_divisor_r_Mcount_cntr_reg5_SW0_SW0 : X_LUT4
    generic map(
      INIT => X"9557",
      LOC => "SLICE_X48Y53"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(5),
      ADR1 => i_divisor_r_cntr_reg(0),
      ADR2 => i_divisor_r_cntr_reg(2),
      ADR3 => i_divisor_r_cntr_reg(3),
      O => i_divisor_r_Mcount_cntr_reg5_SW0_SW0_O_pack_3
    );
  i_divisor_r_Mcount_cntr_reg5_SW0 : X_LUT4
    generic map(
      INIT => X"0FAA",
      LOC => "SLICE_X48Y53"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(5),
      ADR1 => VCC,
      ADR2 => i_divisor_r_Mcount_cntr_reg5_SW0_SW0_O,
      ADR3 => i_divisor_r_cntr_reg(1),
      O => N57
    );
  i_divisor_r_cntr_reg_5 : X_FF
    generic map(
      LOC => "SLICE_X48Y53",
      INIT => '0'
    )
    port map (
      I => i_divisor_r_cntr_reg_5_DXMUX_2794,
      CE => i_divisor_r_cntr_reg_5_CEINV_2776,
      CLK => i_divisor_r_cntr_reg_5_CLKINV_2777,
      SET => GND,
      RST => i_divisor_r_cntr_reg_5_FFX_RSTAND_2800,
      O => i_divisor_r_cntr_reg(5)
    );
  i_divisor_r_cntr_reg_5_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X48Y53",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_r_cntr_reg_5_FFX_RSTAND_2800
    );
  i_divisor_d_cntr_reg_1 : X_FF
    generic map(
      LOC => "SLICE_X40Y25",
      INIT => '0'
    )
    port map (
      I => i_divisor_d_cntr_reg_3_DYMUX_2823,
      CE => VCC,
      CLK => i_divisor_d_cntr_reg_3_CLKINV_2812,
      SET => GND,
      RST => i_divisor_d_cntr_reg_3_SRINVNOT,
      O => i_divisor_d_cntr_reg(1)
    );
  i_divisor_d_Mcount_cntr_reg_xor_3_11 : X_LUT4
    generic map(
      INIT => X"7F80",
      LOC => "SLICE_X40Y25"
    )
    port map (
      ADR0 => i_divisor_d_cntr_reg(1),
      ADR1 => i_divisor_d_cntr_reg(0),
      ADR2 => i_divisor_d_cntr_reg(2),
      ADR3 => i_divisor_d_cntr_reg(3),
      O => i_divisor_d_Mcount_cntr_reg3
    );
  i_divisor_d_cntr_reg_3 : X_FF
    generic map(
      LOC => "SLICE_X40Y25",
      INIT => '0'
    )
    port map (
      I => i_divisor_d_cntr_reg_3_DXMUX_2837,
      CE => VCC,
      CLK => i_divisor_d_cntr_reg_3_CLKINV_2812,
      SET => GND,
      RST => i_divisor_d_cntr_reg_3_SRINVNOT,
      O => i_divisor_d_cntr_reg(3)
    );
  i_dp_i_cnt_mod_r_cnt_in_2 : X_FF
    generic map(
      LOC => "SLICE_X45Y89",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in_3_DYMUX_2867,
      CE => i_dp_i_cnt_mod_r_cnt_in_3_CEINV_2856,
      CLK => i_dp_i_cnt_mod_r_cnt_in_3_CLKINV_2857,
      SET => GND,
      RST => i_dp_i_cnt_mod_r_cnt_in_3_SRINVNOT,
      O => i_dp_i_cnt_mod_r_cnt_in(2)
    );
  i_divisor_r_clk_out_reg_cmp_eq0000 : X_LUT4
    generic map(
      INIT => X"0010",
      LOC => "SLICE_X46Y52"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(2),
      ADR1 => i_divisor_r_cntr_reg(0),
      ADR2 => i_divisor_r_cntr_reg(1),
      ADR3 => i_divisor_r_clk_out_reg_cmp_eq0000_SW0_O,
      O => i_divisor_r_clk_out_reg_cmp_eq0000_2621
    );
  i_cu_current_state_FSM_FFd1_In96 : X_LUT4
    generic map(
      INIT => X"CE0A",
      LOC => "SLICE_X53Y79"
    )
    port map (
      ADR0 => i_cu_current_state_FSM_FFd1_917,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => seqfin_0,
      ADR3 => i_cu_current_state_FSM_FFd1_In96_SW0_O,
      O => i_cu_current_state_FSM_FFd1_In
    );
  i_cu_current_state_FSM_FFd1 : X_FF
    generic map(
      LOC => "SLICE_X53Y79",
      INIT => '0'
    )
    port map (
      I => i_cu_current_state_FSM_FFd1_DXMUX_2652,
      CE => VCC,
      CLK => i_cu_current_state_FSM_FFd1_CLKINV_2635,
      SET => GND,
      RST => i_cu_current_state_FSM_FFd1_FFX_RSTAND_2657,
      O => i_cu_current_state_FSM_FFd1_917
    );
  i_cu_current_state_FSM_FFd1_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X53Y79",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_cu_current_state_FSM_FFd1_FFX_RSTAND_2657
    );
  i_divisor_l_Mcount_cntr_reg_xor_5_1 : X_LUT4
    generic map(
      INIT => X"286C",
      LOC => "SLICE_X28Y50"
    )
    port map (
      ADR0 => i_divisor_l_cntr_reg(4),
      ADR1 => i_divisor_l_cntr_reg(5),
      ADR2 => N54_0,
      ADR3 => i_divisor_l_Mcount_cntr_reg_xor_5_1_SW1_O,
      O => i_divisor_l_Mcount_cntr_reg5
    );
  i_divisor_l_cntr_reg_5 : X_FF
    generic map(
      LOC => "SLICE_X28Y50",
      INIT => '0'
    )
    port map (
      I => i_divisor_l_cntr_reg_5_DXMUX_2687,
      CE => VCC,
      CLK => i_divisor_l_cntr_reg_5_CLKINV_2671,
      SET => GND,
      RST => i_divisor_l_cntr_reg_5_FFX_RSTAND_2692,
      O => i_divisor_l_cntr_reg(5)
    );
  i_divisor_l_cntr_reg_5_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X28Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_l_cntr_reg_5_FFX_RSTAND_2692
    );
  i_divisor_d_Mcount_cntr_reg_xor_0_1 : X_LUT4
    generic map(
      INIT => X"0E0F",
      LOC => "SLICE_X40Y24"
    )
    port map (
      ADR0 => i_divisor_d_cntr_reg(1),
      ADR1 => i_divisor_d_cntr_reg(3),
      ADR2 => i_divisor_d_cntr_reg(0),
      ADR3 => i_divisor_d_Mcount_cntr_reg_xor_0_1_SW0_O,
      O => i_divisor_d_Mcount_cntr_reg
    );
  i_divisor_d_cntr_reg_0 : X_FF
    generic map(
      LOC => "SLICE_X40Y24",
      INIT => '0'
    )
    port map (
      I => i_divisor_d_cntr_reg_0_DXMUX_2722,
      CE => VCC,
      CLK => i_divisor_d_cntr_reg_0_CLKINV_2704,
      SET => GND,
      RST => i_divisor_d_cntr_reg_0_FFX_RSTAND_2727,
      O => i_divisor_d_cntr_reg(0)
    );
  i_divisor_d_cntr_reg_0_FFX_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X40Y24",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_d_cntr_reg_0_FFX_RSTAND_2727
    );
  i_dp_i_cnt_mod_l_Mcount_cnt_in_xor_3_11 : X_LUT4
    generic map(
      INIT => X"68CC",
      LOC => "SLICE_X36Y68"
    )
    port map (
      ADR0 => i_dp_i_cnt_mod_l_cnt_in(1),
      ADR1 => i_dp_i_cnt_mod_l_cnt_in(3),
      ADR2 => i_dp_i_cnt_mod_l_cnt_in(2),
      ADR3 => i_dp_i_cnt_mod_l_cnt_in(0),
      O => i_dp_i_cnt_mod_l_Mcount_cnt_in3
    );
  i_dp_i_cnt_mod_l_cnt_in_3 : X_FF
    generic map(
      LOC => "SLICE_X36Y68",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in_3_DXMUX_2996,
      CE => i_dp_i_cnt_mod_l_cnt_in_3_CEINV_2970,
      CLK => i_dp_i_cnt_mod_l_cnt_in_3_CLKINV_2971,
      SET => GND,
      RST => i_dp_i_cnt_mod_l_cnt_in_3_SRINVNOT,
      O => i_dp_i_cnt_mod_l_cnt_in(3)
    );
  i_cu_current_state_FSM_FFd3_In1 : X_LUT4
    generic map(
      INIT => X"00FC",
      LOC => "SLICE_X42Y85"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd3_894,
      ADR2 => inib_IBUF_970,
      ADR3 => i_cu_current_state_FSM_FFd2_895,
      O => i_cu_current_state_FSM_FFd3_In
    );
  i_cu_current_state_FSM_FFd3 : X_FF
    generic map(
      LOC => "SLICE_X42Y85",
      INIT => '0'
    )
    port map (
      I => i_cu_current_state_FSM_FFd3_DYMUX_3020,
      CE => VCC,
      CLK => i_cu_current_state_FSM_FFd3_CLKINV_3010,
      SET => GND,
      RST => i_cu_current_state_FSM_FFd3_FFY_RSTAND_3025,
      O => i_cu_current_state_FSM_FFd3_894
    );
  i_cu_current_state_FSM_FFd3_FFY_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X42Y85",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_cu_current_state_FSM_FFd3_FFY_RSTAND_3025
    );
  i_dp_i_cnt_seconds_cnt_in_2 : X_FF
    generic map(
      LOC => "SLICE_X42Y73",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_seconds_cnt_in_3_DYMUX_3048,
      CE => VCC,
      CLK => i_dp_i_cnt_seconds_cnt_in_3_CLKINV_3038,
      SET => GND,
      RST => i_dp_i_cnt_seconds_cnt_in_3_SRINV_3039,
      O => i_dp_i_cnt_seconds_cnt_in(2)
    );
  i_dp_i_cnt_seconds_Mcount_cnt_in_xor_3_11 : X_LUT4
    generic map(
      INIT => X"6A8A",
      LOC => "SLICE_X42Y73"
    )
    port map (
      ADR0 => i_dp_i_cnt_seconds_cnt_in(3),
      ADR1 => i_dp_i_cnt_seconds_cnt_in(2),
      ADR2 => i_dp_i_cnt_seconds_cnt_in(0),
      ADR3 => i_dp_i_cnt_seconds_cnt_in(1),
      O => i_dp_i_cnt_seconds_Mcount_cnt_in3
    );
  i_dp_i_cnt_seconds_cnt_in_3 : X_FF
    generic map(
      LOC => "SLICE_X42Y73",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_seconds_cnt_in_3_DXMUX_3062,
      CE => VCC,
      CLK => i_dp_i_cnt_seconds_cnt_in_3_CLKINV_3038,
      SET => GND,
      RST => i_dp_i_cnt_seconds_cnt_in_3_SRINV_3039,
      O => i_dp_i_cnt_seconds_cnt_in(3)
    );
  i_divisor_r_cntr_reg_2 : X_FF
    generic map(
      LOC => "SLICE_X46Y53",
      INIT => '0'
    )
    port map (
      I => i_divisor_r_cntr_reg_3_DYMUX_3090,
      CE => VCC,
      CLK => i_divisor_r_cntr_reg_3_CLKINV_3080,
      SET => GND,
      RST => i_divisor_r_cntr_reg_3_SRINVNOT,
      O => i_divisor_r_cntr_reg(2)
    );
  i_divisor_r_Mcount_cntr_reg32 : X_LUT4
    generic map(
      INIT => X"7F80",
      LOC => "SLICE_X46Y53"
    )
    port map (
      ADR0 => i_divisor_r_cntr_reg(2),
      ADR1 => i_divisor_r_cntr_reg(1),
      ADR2 => i_divisor_r_cntr_reg(0),
      ADR3 => i_divisor_r_cntr_reg(3),
      O => i_divisor_r_Mcount_cntr_reg3
    );
  i_divisor_r_cntr_reg_3 : X_FF
    generic map(
      LOC => "SLICE_X46Y53",
      INIT => '0'
    )
    port map (
      I => i_divisor_r_cntr_reg_3_DXMUX_3104,
      CE => VCC,
      CLK => i_divisor_r_cntr_reg_3_CLKINV_3080,
      SET => GND,
      RST => i_divisor_r_cntr_reg_3_SRINVNOT,
      O => i_divisor_r_cntr_reg(3)
    );
  i_dp_right_1_11 : X_LUT4
    generic map(
      INIT => X"0F0C",
      LOC => "SLICE_X47Y94"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => N93,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => right_1_OBUF_3132
    );
  i_dp_i_cnt_mod_r_Mcount_cnt_in_xor_1_11 : X_LUT4
    generic map(
      INIT => X"3C34",
      LOC => "SLICE_X44Y89"
    )
    port map (
      ADR0 => i_dp_i_cnt_mod_r_cnt_in(3),
      ADR1 => i_dp_i_cnt_mod_r_cnt_in(0),
      ADR2 => i_dp_i_cnt_mod_r_cnt_in(1),
      ADR3 => i_dp_i_cnt_mod_r_cnt_in(2),
      O => i_dp_i_cnt_mod_r_Mcount_cnt_in1
    );
  i_dp_i_cnt_mod_r_cnt_in_1 : X_FF
    generic map(
      LOC => "SLICE_X44Y89",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in_0_DYMUX_3155,
      CE => i_dp_i_cnt_mod_r_cnt_in_0_CEINV_3145,
      CLK => i_dp_i_cnt_mod_r_cnt_in_0_CLKINV_3146,
      SET => GND,
      RST => i_dp_i_cnt_mod_r_cnt_in_0_SRINVNOT,
      O => i_dp_i_cnt_mod_r_cnt_in(1)
    );
  i_dp_i_cnt_mod_r_cnt_in_0 : X_FF
    generic map(
      LOC => "SLICE_X44Y89",
      INIT => '0'
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in_0_DXMUX_3164,
      CE => i_dp_i_cnt_mod_r_cnt_in_0_CEINV_3145,
      CLK => i_dp_i_cnt_mod_r_cnt_in_0_CLKINV_3146,
      SET => GND,
      RST => i_dp_i_cnt_mod_r_cnt_in_0_SRINVNOT,
      O => i_dp_i_cnt_mod_r_cnt_in(0)
    );
  i_dp_leds_lose_0 : X_FF
    generic map(
      LOC => "SLICE_X54Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_1_DYMUX_3181,
      CE => VCC,
      CLK => i_dp_leds_lose_1_CLKINV_3178,
      SET => GND,
      RST => i_dp_leds_lose_1_SRINVNOT,
      O => i_dp_leds_lose(0)
    );
  i_dp_leds_lose_1 : X_FF
    generic map(
      LOC => "SLICE_X54Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_1_DXMUX_3189,
      CE => VCC,
      CLK => i_dp_leds_lose_1_CLKINV_3178,
      SET => i_dp_leds_lose_1_SRINVNOT,
      RST => GND,
      O => i_dp_leds_lose(1)
    );
  i_dp_right_5_11 : X_LUT4
    generic map(
      INIT => X"F0C0",
      LOC => "SLICE_X43Y94"
    )
    port map (
      ADR0 => VCC,
      ADR1 => i_cu_current_state_FSM_FFd2_895,
      ADR2 => N85,
      ADR3 => i_cu_current_state_FSM_FFd3_894,
      O => right_5_OBUF_3216
    );
  i_dp_leds_lose_2 : X_FF
    generic map(
      LOC => "SLICE_X55Y84",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_3_DYMUX_3228,
      CE => VCC,
      CLK => i_dp_leds_lose_3_CLKINV_3225,
      SET => GND,
      RST => i_dp_leds_lose_3_SRINVNOT,
      O => i_dp_leds_lose(2)
    );
  i_dp_leds_lose_3 : X_FF
    generic map(
      LOC => "SLICE_X55Y84",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_3_DXMUX_3236,
      CE => VCC,
      CLK => i_dp_leds_lose_3_CLKINV_3225,
      SET => i_dp_leds_lose_3_SRINVNOT,
      RST => GND,
      O => i_dp_leds_lose(3)
    );
  i_dp_leds_lose_4 : X_FF
    generic map(
      LOC => "SLICE_X50Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_5_DYMUX_3251,
      CE => VCC,
      CLK => i_dp_leds_lose_5_CLKINV_3248,
      SET => GND,
      RST => i_dp_leds_lose_5_SRINVNOT,
      O => i_dp_leds_lose(4)
    );
  i_dp_leds_lose_5 : X_FF
    generic map(
      LOC => "SLICE_X50Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_5_DXMUX_3259,
      CE => VCC,
      CLK => i_dp_leds_lose_5_CLKINV_3248,
      SET => i_dp_leds_lose_5_SRINVNOT,
      RST => GND,
      O => i_dp_leds_lose(5)
    );
  i_dp_leds_lose_6 : X_FF
    generic map(
      LOC => "SLICE_X49Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_7_DYMUX_3274,
      CE => VCC,
      CLK => i_dp_leds_lose_7_CLKINV_3271,
      SET => GND,
      RST => i_dp_leds_lose_7_SRINVNOT,
      O => i_dp_leds_lose(6)
    );
  i_dp_leds_lose_7 : X_FF
    generic map(
      LOC => "SLICE_X49Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_lose_7_DXMUX_3282,
      CE => VCC,
      CLK => i_dp_leds_lose_7_CLKINV_3271,
      SET => i_dp_leds_lose_7_SRINVNOT,
      RST => GND,
      O => i_dp_leds_lose(7)
    );
  i_dp_leds_wait_0 : X_FF
    generic map(
      LOC => "SLICE_X55Y82",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_1_DYMUX_3297,
      CE => VCC,
      CLK => i_dp_leds_wait_1_CLKINV_3294,
      SET => GND,
      RST => i_dp_leds_wait_1_SRINVNOT,
      O => i_dp_leds_wait(0)
    );
  i_divisor_l_clk_out_reg : X_FF
    generic map(
      LOC => "SLICE_X30Y50",
      INIT => '0'
    )
    port map (
      I => i_divisor_l_clk_out_reg_DYMUX_3392,
      CE => i_divisor_l_clk_out_reg_CEINV_3388,
      CLK => i_divisor_l_clk_out_reg_CLKINV_3389,
      SET => GND,
      RST => i_divisor_l_clk_out_reg_FFY_RSTAND_3398,
      O => i_divisor_l_clk_out_reg_929
    );
  i_divisor_l_clk_out_reg_FFY_RSTAND : X_INV
    generic map(
      LOC => "SLICE_X30Y50",
      PATHPULSE => 526 ps
    )
    port map (
      I => rst_IBUF_892,
      O => i_divisor_l_clk_out_reg_FFY_RSTAND_3398
    );
  i_dp_leds_wait_6 : X_FF
    generic map(
      LOC => "SLICE_X51Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_7_DYMUX_3421,
      CE => VCC,
      CLK => i_dp_leds_wait_7_CLKINV_3418,
      SET => GND,
      RST => i_dp_leds_wait_7_SRINVNOT,
      O => i_dp_leds_wait(6)
    );
  i_dp_leds_wait_7 : X_FF
    generic map(
      LOC => "SLICE_X51Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_7_DXMUX_3429,
      CE => VCC,
      CLK => i_dp_leds_wait_7_CLKINV_3418,
      SET => GND,
      RST => i_dp_leds_wait_7_SRINVNOT,
      O => i_dp_leds_wait(7)
    );
  i_dp_leds_wait_8 : X_FF
    generic map(
      LOC => "SLICE_X52Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_9_DYMUX_3445,
      CE => VCC,
      CLK => i_dp_leds_wait_9_CLKINV_3442,
      SET => GND,
      RST => i_dp_leds_wait_9_SRINVNOT,
      O => i_dp_leds_wait(8)
    );
  i_dp_leds_wait_9 : X_FF
    generic map(
      LOC => "SLICE_X52Y83",
      INIT => '0'
    )
    port map (
      I => i_dp_leds_wait_9_DXMUX_3453,
      CE => VCC,
      CLK => i_dp_leds_wait_9_CLKINV_3442,
      SET => GND,
      RST => i_dp_leds_wait_9_SRINVNOT,
      O => i_dp_leds_wait(9)
    );
  right_0_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD60",
      PATHPULSE => 526 ps
    )
    port map (
      I => right_0_OBUF_1374,
      O => right_0_O
    );
  right_1_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD59",
      PATHPULSE => 526 ps
    )
    port map (
      I => right_1_OBUF_3132,
      O => right_1_O
    );
  right_2_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD58",
      PATHPULSE => 526 ps
    )
    port map (
      I => right_2_OBUF_1059,
      O => right_2_O
    );
  right_3_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD57",
      PATHPULSE => 526 ps
    )
    port map (
      I => right_3_OBUF_1265,
      O => right_3_O
    );
  right_4_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD56",
      PATHPULSE => 526 ps
    )
    port map (
      I => right_4_OBUF_1439,
      O => right_4_O
    );
  leds_0_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD113",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_0_OBUF_F5MUX_2139,
      O => leds_0_O
    );
  right_5_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD55",
      PATHPULSE => 526 ps
    )
    port map (
      I => right_5_OBUF_3216,
      O => right_5_O
    );
  leds_1_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD114",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_1_OBUF_F5MUX_2114,
      O => leds_1_O
    );
  right_6_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD54",
      PATHPULSE => 526 ps
    )
    port map (
      I => right_6_OBUF_1152,
      O => right_6_O
    );
  leds_2_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD83",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_2_OBUF_F5MUX_2164,
      O => leds_2_O
    );
  leds_3_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD82",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_3_OBUF_F5MUX_2189,
      O => leds_3_O
    );
  leds_4_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD66",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_4_OBUF_F5MUX_2214,
      O => leds_4_O
    );
  leds_5_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD65",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_5_OBUF_F5MUX_2239,
      O => leds_5_O
    );
  leds_6_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD64",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_6_OBUF_F5MUX_2300,
      O => leds_6_O
    );
  leds_7_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD63",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_7_OBUF_F5MUX_2325,
      O => leds_7_O
    );
  leds_8_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD62",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_8_OBUF_F5MUX_2350,
      O => leds_8_O
    );
  leds_9_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD61",
      PATHPULSE => 526 ps
    )
    port map (
      I => leds_9_OBUF_F5MUX_2375,
      O => leds_9_O
    );
  left_0_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD50",
      PATHPULSE => 526 ps
    )
    port map (
      I => left_0_OBUF_1128,
      O => left_0_O
    );
  left_1_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD49",
      PATHPULSE => 526 ps
    )
    port map (
      I => left_1_OBUF_1289,
      O => left_1_O
    );
  left_2_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD48",
      PATHPULSE => 526 ps
    )
    port map (
      I => left_2_OBUF_1463,
      O => left_2_O
    );
  left_3_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD47",
      PATHPULSE => 526 ps
    )
    port map (
      I => left_3_OBUF_1224,
      O => left_3_O
    );
  left_4_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD46",
      PATHPULSE => 526 ps
    )
    port map (
      I => left_4_OBUF_1176,
      O => left_4_O
    );
  left_5_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD45",
      PATHPULSE => 526 ps
    )
    port map (
      I => left_5_OBUF_1350,
      O => left_5_O
    );
  left_6_OUTPUT_OFF_OMUX : X_BUF
    generic map(
      LOC => "PAD44",
      PATHPULSE => 526 ps
    )
    port map (
      I => left_6_OBUF_1522,
      O => left_6_O
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA : X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB : X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(3),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(2),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(1),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(0),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(3),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(2),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(1),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q
    );
  NlwBufferBlock_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_l_cnt_in(0),
      O => 
NlwBufferSignal_i_dp_i_rom_left_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA : X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKA
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB : X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => clk_BUFGP,
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_CLKB
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(3),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_4_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(2),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_3_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(1),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_2_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(0),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRA_1_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(3),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_4_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(2),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_3_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(1),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_2_Q
    );
  NlwBufferBlock_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q : 
X_BUF
    generic map(
      PATHPULSE => 526 ps
    )
    port map (
      I => i_dp_i_cnt_mod_r_cnt_in(0),
      O => 
NlwBufferSignal_i_dp_i_rom_right_U0_xst_blk_mem_generator_gnativebmg_native_blk_mem_gen_valid_cstr_ramloop_0_ram_r_s3_init_ram_spram_ram_ADDRB_1_Q
    );
  NlwBlock_slots_top_VCC : X_ONE
    port map (
      O => VCC
    );
  NlwBlock_slots_top_GND : X_ZERO
    port map (
      O => GND
    );
  NlwBlockROC : X_ROC
    generic map (ROC_WIDTH => 100 ns)
    port map (O => GSR);
  NlwBlockTOC : X_TOC
    port map (O => GTS);

end Structure;

