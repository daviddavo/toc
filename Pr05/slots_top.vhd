----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:01:58 11/30/2018 
-- Design Name: 
-- Module Name:    slots_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use WORK.COMMON.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity slots_top is
port (
	clk	: in std_logic;
	rst	: in std_logic;
	
	inib	: in std_logic;
	endb	: in std_logic;
	
	leds	: out std_logic_vector(9 downto 0);
	left	: out std_logic_vector(6 downto 0);
	right	: out std_logic_vector(6 downto 0)
);
end slots_top;

architecture Behavioral of slots_top is
	component slots_cu
	port (
		clk	: in std_logic;
		rst	: in std_logic;
		inib	: in std_logic;
		endb	: in std_logic;
		
		-- STATUS SIGNALS
		same	: in std_logic;
		seqfin	: in std_logic;
		
		-- CONTROL SIGNALS
		spin		: out std_logic;
		cntrst	: out std_logic;
		seq		: out t_sequence
		);
	end component;
	
	component slots_dp
	port (
		clk_mem	: in std_logic;
		-- Led display clock (1 Hz)
		clk_d		: in std_logic;
		-- Left display's clock
		clk_l		: in std_logic;
		-- Right display's cock
		clk_r		: in std_logic;
		
		-- RESET
		rst		: in std_logic;

		-- CONTROL SIGNALS
		spin		: in std_logic;
		cntrst	: in std_logic;
		seq		: in t_sequence;

		-- STATUS SIGNALS
		same	: out std_logic;
		seqfin	: out std_logic;
		
		-- OUTPUTS
		left	: out std_logic_vector(6 downto 0);
		right	: out	std_logic_vector(6 downto 0);
		leds  : out std_logic_vector(9 downto 0)
	);
	end component;
	
	component divisor
	generic ( N : integer );
	port (
		rst			: in std_logic;
		clk_100mhz	: in std_logic;
		clk_out		: out std_logic
	);
	end component;
	
	component debouncer is
	port (
		rst             : in  std_logic;
		clk             : in  std_logic;
		x               : in  std_logic;
		xdeb            : out std_logic;
		xdebfallingedge : out std_logic;
		xdebrisingedge  : out std_logic
	);
	end component;

	-- Clock networks
	signal clk_l	: std_logic;
	signal clk_r	: std_logic;
	signal clk_d	: std_logic;
	signal clk_1mhz: std_logic;
	
	-- STATUS SIGNALS
	signal same		: std_logic;
	signal seqfin	: std_logic;
	
	-- CONTROL SIGNALS
	signal seq		: t_sequence;
	signal spin		: std_logic;
	signal cntrst	: std_logic;
	
	-- DEBOUNCED
	signal inideb	: std_logic;
	signal enddeb	: std_logic;
begin
	i_divisor_d	: divisor
	-- Should be 10**6
	generic map ( N => 10**6 )
	port map (
		rst			=> rst,
		clk_100mhz	=> clk,
		clk_out		=> clk_d
	);
	
	i_divisor_l : divisor
	generic map ( N => 40000 )
	port map (
		rst 			=> rst,
		clk_100mhz	=> clk,
		clk_out		=> clk_l
	);
	
	i_divisor_r : divisor
	generic map ( N => 500000 )
	port map (
		rst			=> rst,
		clk_100mhz	=> clk,
		clk_out		=> clk_r
	);
	
	-- TODO: Add clock network for 1hz
	i_divisor_r : divisor
	generic map ( N => 10**7 )
	port map (
		rst			=> rst,
		clk_100mhz	=> clk,
		clk_out		=> clk_1mhz
	);
	
	i_cu : slots_cu
	port map (
		clk			=> clk,
		rst			=> rst,
		inib			=> inideb,
		endb			=> enddeb,
		
		-- STATUS SIGNALS
		same 			=> same,
		seqfin			=> seqfin,
		
		-- CONTROL SIGNALS
		spin			=> spin,
		cntrst		=> cntrst,
		seq			=> seq
	);
	
	i_dp	: slots_dp
	port map (
		clk_mem		=> clk,
		clk_d 		=> clk_d,
		clk_l			=> clk_l,
		clk_r			=> clk_r,
		
		rst			=> rst,

		-- CONTROL SIGNALS
		spin			=> spin,
		cntrst		=> cntrst,
		seq			=> seq,

		-- STATUS SIGNALS
		same			=> same,
		seqfin			=> seqfin,
		
		-- OUTPUTS
		left			=> left,
		right			=> right,
		leds			=> leds
	);
	
	-- Only in simulation
	inideb	<= inib;
	enddeb	<= endb;
--	
--	i_deb_ini		: debouncer
--	port map (
--		rst					=> rst,
--		clk					=> clk,
--		x						=> inib,
--		xdebfallingedge	=>	inideb,
--		xdeb					=> open,
--		xdebrisingedge		=> open
--	);
--	
--	i_deb_end		: debouncer
--	port map (
--		rst					=> rst,
--		clk					=> clk,
--		x						=> endb,
--		xdebfallingedge	=> enddeb,
--		xdeb					=> open,
--		xdebrisingedge		=> open
--	);
end Behavioral;
