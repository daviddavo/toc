----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:15:36 11/29/2018 
-- Design Name: 
-- Module Name:    decoder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-- use IEEE.MATH_REAL.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder is
generic ( N : integer := 2 );
port (
	input		: in unsigned(N-1 downto 0);
	output	: out std_logic_vector(2**N-1 downto 0)
);
end decoder;
architecture Behavioral of decoder is
	signal aux	: std_logic_vector(2**N-1 downto 0);
begin
	aux <= (others => '0');
	aux(to_integer(input)) <= '1';
	
	output <= aux;
end Behavioral;