----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:17:26 11/11/2018 
-- Design Name: 
-- Module Name:    uc - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity uc is
port (
	clk		: in std_logic;
	rst		: in std_logic;
	ini		: in std_logic;	-- CONTROL INPUT
	done		: out std_logic;	-- CONTROL OUTPUT
	status	: in std_logic;
	ctrl		: out std_logic_vector(4 downto 0)
);
end uc;
architecture Behavioral of uc is
	type t_st is (s_done, s_init, s_idle, s_idle_b, s_odd, s_even);
	signal current_state, next_state	: t_st := s_done;
	
	-- ESTADOS INTERNOS
	signal comp	: std_logic;
begin
	comp <= status;

	-- Status signals should be on the sensivity list
	p_next_state : process(current_state, ini, comp) is
	begin
		case current_state is
			when s_done =>
				if ini = '1' then
					next_state <= s_init;
				else
					next_state <= s_done;
				end if;
			when s_init =>
				next_state <= s_idle;
			when s_idle =>
				if comp = '0' then
					next_state <= s_done;
				else
					next_state <= s_idle_b;
				end if;
			when s_idle_b =>
				if comp = '1' then
					next_state <= s_odd;
				else
					next_state <= s_even;
				end if;
			when s_odd =>
				next_state <= s_idle;
			when s_even =>
				next_state <= s_idle;
			when others => null;
		end case;
	end process p_next_state;
	
	p_outputs : process (current_state)
	----------------------------------------------------------------
	-- Definicion de cada una de las seales de control
	-- Tambien funcionaria con 5 seales, distintas, pero bueno
	----------------------------------------------------------------
	constant c_sh				: std_logic_vector(4 downto 0) := "10000";
	constant c_ld				: std_logic_vector(4 downto 0) := "01000";
	constant c_countDown		: std_logic_vector(4 downto 0) := "00100";
	constant c_acc				: std_logic_vector(4 downto 0) := "00010";
	constant c_mux				: std_logic_vector(4 downto 0) := "00001";
	begin
		case current_state is
			when s_done =>
				done <= '1';
				ctrl <= (others => '0');
			when s_init =>
				done <= '0';
				ctrl <= c_ld;
			when s_idle =>
				done <= '0';
				ctrl <= (others => '0');
			when s_idle_b =>
				done <= '0';
				ctrl <= c_mux;
			when s_odd =>
				done <= '0';
				ctrl <= c_acc or c_countDown or c_sh;
			when s_even =>
				done <= '0';
				ctrl <= c_countDown or c_sh;
			when others => null;
		end case;
	end process p_outputs;
	
	p_status_reg : process (clk, rst) is
	begin
		if rst = '0' then
			current_state <= s_done;
		elsif rising_edge(clk) then
			current_state <= next_state;
		end if;
	end process p_status_reg;

end Behavioral;

