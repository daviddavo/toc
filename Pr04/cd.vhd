----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:35:32 11/11/2018 
-- Design Name: 
-- Module Name:    cd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use IEEE.STD_LOGIC_MISC.or_reduce;

entity cd is 
generic ( N: integer );
port (
	clk		: in std_logic;
	rst		: in std_logic;
	ctrl		: in std_logic_vector(4 downto 0);
	status	: out std_logic;
	a_in		: in std_logic_vector(N-1 downto 0);	-- DATA INPUT
	b_in		: in std_logic_vector(N-1 downto 0);	-- DATA INPUT
	z			: out std_logic_vector(2*N-1 downto 0)	-- DATA OUTPUT
);
end cd;

architecture Behavioral of cd is
	component piso_4b_lsr
	generic ( N : integer );
	port(
		x 		: in std_logic_vector(N-1 downto 0);
		clk 	: in std_logic;
		rst 	: in std_logic;
		load 	: in std_logic;
		shift : in std_logic;          
		z 		: out std_logic
	);
	end component;
	
	component pipo_8b_lsl
	generic ( N : integer );
	port(
		x 		: in std_logic_vector(N-1 downto 0);
		clk 	: in std_logic;
		rst 	: in std_logic;
		load 	: in std_logic;
		shift : in std_logic;          
		z 		: out std_logic_vector(N-1 downto 0)
	);
	end component;
	
	component counter_4b_down
	generic ( N : integer );
	port (
		x		: in std_logic_vector(N-1 downto 0);
		load	: in std_logic;
		clk	: in std_logic;
		cd		: in std_logic;
		z		: out std_logic_vector(N-1 downto 0)
	);
	end component;
	
	component accumulator_8b
	generic ( N : integer );
	port (
		x				: in std_logic_vector(N-1 downto 0);
		y				: in std_logic_vector(N-1 downto 0);
		clk, load	: in std_logic;
		acc			: in std_logic;
		z				: out std_logic_vector(N-1 downto 0)
	);
	end component;
	
	component mux_2s
	port (
		x, y, s	: in std_logic;
		z			: out std_logic
	);
	end component;

	signal a			: std_logic_vector(2*N-1 downto 0);	-- operandos del multiplicador (salida de registros)
	signal b			: std_logic;							-- b es std_logic pues lo he unido a un registro piso (solo necesitamos el ultimo bit)
	constant cnt_length	: integer := integer(ceil(log2(real(N))))+1;
	signal cnt		: std_logic_vector(cnt_length-1 downto 0);
	signal rsrinput: std_logic_vector(2*N-1 downto 0);

	-- Seales de control (internas)
	signal cr_sh			: std_logic;
	signal cr_ld			: std_logic;
	signal cr_countDown	: std_logic;
	signal cr_acc			: std_logic;
	signal cr_mux			: std_logic;
	
	-- Seales de estado (internas)
	signal comp	: std_logic;
begin
	(cr_sh,
	cr_ld,
	cr_countDown,
	cr_acc,
	cr_mux
	) <= ctrl;
	
	status <= (comp);

	----------------------------------------------------------------
	-- Registro desplazamiento derecha (piso)
	----------------------------------------------------------------
	i_piso_4b_lsr : piso_4b_lsr 
	generic map ( N => N )
	port map (
		x		=> b_in,
		clk	=> clk,
		rst	=> rst,
		shift	=> cr_sh,
		load	=> cr_ld,
		z		=> b
	);
	
	----------------------------------------------------------------
	-- Registro desplazamiento izquierda
	----------------------------------------------------------------
	rsrinput <= std_logic_vector(to_unsigned(0, N))&a_in;
	
	i_pipo_8b_lsl : pipo_8b_lsl 
	generic map ( N => 2*N )
	port map (
		x		=> rsrinput,
		clk	=> clk,
		rst	=> rst,
		shift	=> cr_sh,
		load	=> cr_ld,
		z		=> a
	);
	
	----------------------------------------------------------------
	-- Contador inverso de n
	----------------------------------------------------------------
	i_counter_4b_down : counter_4b_down
	generic map ( N => cnt_length)
	port map (
		x		=> std_logic_vector(to_unsigned(N, cnt_length)),
		load	=> cr_ld,
		clk	=> clk,
		cd		=> cr_countDown,
		z		=> cnt
	);
	
	----------------------------------------------------------------
	-- Acumulador acc
	----------------------------------------------------------------
	i_accumulator_8b : accumulator_8b 
	generic map ( N => 2*N )
	port map (
		x		=> std_logic_vector(to_unsigned(0, 2*N)),
		y		=> a,
		load	=> cr_ld,
		acc	=> cr_acc,
		clk	=> clk,
		z		=> z
	);
	
	----------------------------------------------------------------
	-- Comparador de n a 0
	----------------------------------------------------------------
	-- Ver implementacin directa sin seales en i_mux_2s
	-- y => or_reduce(n);
	
	----------------------------------------------------------------
	-- Multiplexor 2 entradas
	----------------------------------------------------------------
	i_mux_2s : mux_2s port map (
		x =>	b,
		y =>	or_reduce(cnt),
		s =>	cr_mux,
		z =>	comp
	);

end Behavioral;

