----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:33:40 11/11/2018 
-- Design Name: 
-- Module Name:    pipo_8b_lsr - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity piso_4b_lsr is 
generic ( N: integer );
port (
	x				: in std_logic_vector(N-1 downto 0);
	z				: out std_logic;
	clk, rst		: in std_logic;
	load, shift	: in std_logic
);
end piso_4b_lsr;

architecture Behavioral of piso_4b_lsr is
signal mem_int	: std_logic_vector(N-1 downto 0);
begin
	z <= mem_int(0);

	p_piso_lsr	: process (clk, rst) is
	begin
		if rst = '0' then
			mem_int <= (others => '0');
		elsif rising_edge(clk) then
			if load = '1' then
				mem_int <= x;
			elsif shift = '1' then
				mem_int <= '0' & mem_int(N-1 downto 1);
			end if;
		end if;
	end process p_piso_lsr;
end Behavioral;

