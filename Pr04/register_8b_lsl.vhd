----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:14:18 11/11/2018 
-- Design Name: 
-- Module Name:    register_8b_lsl - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity register_8b_lsl is port (
	x				: in std_logic_vector(7 downto 0);
	z				: out std_logic_vector(7 downto 0);
	rst, clk		: in std_logic;
	lsl, load	: in std_logic
);
end register_8b_lsl;
architecture Behavioral of register_8b_lsl is
	signal next_output	:	std_logic_vector(7 downto 0);
begin
	p_register : process (rst, clk) is
	begin
		if rst = '0' then
			next_output <= (others => '0');
		elsif rising_edge(clk) then
			if lsl = '1' then
				next_output <= prev_output(6 downto 0)&'0';
			elsif load = '1' then
				next_output <= x;
			end if;
		end if;			
	end process p_register;

end Behavioral;

