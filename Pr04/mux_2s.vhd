----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    00:01:11 11/12/2018 
-- Design Name: 
-- Module Name:    mux_2s - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mux_2s is port (
	x, y, s	: in std_logic;
	z			: out std_logic
);
end mux_2s;

architecture Behavioral of mux_2s is
begin
	p_mux : process (x, y, s) is
	begin
		if s = '1' then
			z <= x;
		else
			z <= y;
		end if;
	end process p_mux;
end Behavioral;

