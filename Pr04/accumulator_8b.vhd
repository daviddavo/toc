----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    23:40:51 11/11/2018 
-- Design Name: 
-- Module Name:    accumulator_8b - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity accumulator_8b is 
generic ( N : integer );
port (
	x				: in std_logic_vector(N-1 downto 0);
	y				: in std_logic_vector(N-1 downto 0);
	clk, load	: in std_logic;
	acc			: in std_logic;
	z				: out std_logic_vector(N-1 downto 0)
);
end accumulator_8b;
architecture Behavioral of accumulator_8b is
	signal mem_int	: unsigned(N-1 downto 0);
begin
	p_acc : process (clk, load, x, y) is
	begin
		if load = '1' then
			mem_int <= unsigned(x);
		elsif rising_edge(clk) and acc = '1' then
			mem_int <= mem_int + unsigned(y);
		end if;
	end process p_acc;

	z <= std_logic_vector(mem_int);
end Behavioral;

