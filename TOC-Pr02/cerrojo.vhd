----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:18:31 10/15/2018 
-- Design Name: 
-- Module Name:    cerrojo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity cerrojo is
	port (clk 	: in std_logic;
			rst 	: in std_logic;
			intro	: in std_logic;
			input	: in std_logic_vector(7 downto 0);
			suc	: out std_logic;
			lcd	: out std_logic_vector(6 downto 0)
);
end cerrojo;

architecture Behavioral of cerrojo is
	type t_status is (inicial, final, uno, dos, tres);
	signal current_state, next_state	: t_status;
	signal key			: std_logic_vector(7 downto 0);
	signal intentos	: std_logic_vector(3 downto 0);
	signal xdeb			: std_logic;
	signal clk_in		: std_logic;
	-- signal eq 			: std_logic;
	
	component conv_7seg is
	port (
		x			: in std_logic_vector(3 downto 0);
		display	: out std_logic_vector(6 downto 0)
	);
	end component;
	
	component debouncer is
	port (
		rst	: in std_logic;
		clk	: in std_logic;
		x		: in std_logic;
		xdeb	: out std_logic;
		xdebfallingedge	: out std_logic;
		xdebrisingedge		: out std_logic
	);
	end component;
	
	component divisor is
	port (
		rst        : in  std_logic;         -- asynch reset
		clk_100mhz : in  std_logic;         -- 100 MHz input clock
		clk_1hz    : out std_logic          -- 1 Hz output clock
	);
	end component;
	
begin
	i_conv_7seg	: conv_7seg port map (
		x		   => intentos,
		display	=> lcd
	);
	
	-- SOLO IMPLEMENTACION
--	i_debouncer : debouncer port map (
--		rst => rst,
--		x => intro,
--		clk => clk,
--		xdebfallingedge => xdeb
--	);
--	i_divisor	: divisor port map (
--		rst => rst,
--		clk_100mhz => clk,
--		clk_1hz	=> clk_in
--	);
	-- SOLO SIMULACION
	xdeb <= intro;
   clk_in <= clk;
	
	p_clk : process(rst, clk_in)
	begin
		 if rst = '0' then
			 key <= "00000000";
			 current_state <= inicial;
		 elsif rising_edge(clk_in) then
		    current_state <= next_state;
			 if xdeb = '1' and current_state = inicial then
				key <= input;
			 end if;
		 end if;
	end process p_clk;
	
	p_tick : process(current_state, xdeb, input, key)
	begin
		case current_state is
			when inicial =>
				intentos <= "1010";
				suc <= '1';
				if xdeb = '1' then
					next_state <= tres;
				else
					next_state <= current_state;
				end if;
			when uno =>
				intentos <= "0001";
				suc <= '0';
				if xdeb = '1' and input = key then
					next_state <= inicial;
				elsif xdeb = '1' then
					next_state <= final;
				else
					next_state <= current_state;
				end if;
			when dos =>
				intentos <= "0010";
				suc <= '0';
				if xdeb = '1' and input = key then
					next_state <= inicial;
				elsif xdeb = '1' then
					next_state <= uno;
				else
					next_state <= current_state;
				end if;
			when tres =>
				intentos <= "0011";
				suc <= '0';
				if xdeb = '1' and input = key then
					next_state <= inicial;
				elsif xdeb = '1' then
					next_state <= dos;
				else
					next_state <= current_state;
				end if;
			when final =>
				intentos <= "1111";
				suc <= '0';
				next_state <= current_state;
			when others =>
				intentos <= "0000";
				suc <= '1';
				next_state <= inicial;
		end case;
	end process p_tick;
end Behavioral;

