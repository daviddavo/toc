--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   18:22:32 10/16/2018
-- Design Name:   
-- Module Name:   /home/davo/Documents/TOC-Pr02/tb_cerrojo.vhd
-- Project Name:  TOC-Pr02
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: cerrojo
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_cerrojo IS
END tb_cerrojo;
 
ARCHITECTURE behavior OF tb_cerrojo IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT cerrojo
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         intro : IN  std_logic;
         input : IN  std_logic_vector(7 downto 0);
         suc : OUT  std_logic;
         lcd : OUT  std_logic_vector(6 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal intro : std_logic := '0';
   signal input : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal suc : std_logic;
   signal lcd : std_logic_vector(6 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: cerrojo PORT MAP (
          clk => clk,
          rst => rst,
          intro => intro,
          input => input,
          suc => suc,
          lcd => lcd
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      rst <= '0';
		wait for 97 ns;
		input<="11001100";
      wait for 10 ns;
		rst<='1';
		wait for 10 ns;
		intro<='1';
		wait for 10 ns;
		input<="10101010";
		rst<='0';
		wait for 10 ns;
		intro<='0';
		rst<='1';
		wait for 10 ns;
		input<="11000000";
		intro<='1';
		wait for 10 ns;
		input<="01010101";
		intro<='1';
		wait for 30 ns;
		intro<='0';
		input<="11110000";
		wait for 10 ns;
		rst<='0';
		wait for 30 ns;
		rst<='1';
		input<="00110000";
		intro<='1';
		wait for 10 ns;
		input<="00100001";
		intro<='0';
		wait for 10 ns;
		intro<='1';
		wait for 10 ns;
		intro<='0';
		wait for 10 ns;
		intro<='1';
		wait for 10 ns;
		intro<='0';
		wait for 50 ns;
		intro<='1';
		wait for 10 ns;
		intro<='0';
      -- insert stimulus here 

      wait;
   end process;

END;
